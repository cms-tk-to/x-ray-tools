# General notes

- These configuration files have been tested with Allpix2 version 2.4.1.
- The CROC detector model `croc.conf` was derived from the RD53A detector model shipped with the installation of Allpix2 2.4.1 at `share/Allpix/models/rd53a_*.conf`.
- The configuration file for the simulation `sim.conf` was derived from an example presented in the Allpix2 software manual.
- The CROC detector model in this directory is for the square cell geometry. To simulate another geometry, `number_of_pixels` and `pixel_size` are to be modified in `croc.conf`.

# Running on LXPLUS

1. Setup the environment by sourcing the file at one of these paths: `/cvmfs/clicdp.cern.ch/software/allpix-squared/2.4.1/*/setup.sh`, selecting the one fitting for the OS you're using.
2. Run `allpix -c sim.conf`.

# Simulation of the CROC threshold scan with X-rays

1. Run `for THR in {${a}..${b}..${d}} ; do allpix -c sim.conf -o DefaultDigitizer.threshold=${THR}e -o root_file=${THR}e ; done` after setting `a`, `b`, `d` as you like.
2. Run `for ROOF in output/*e.root ; do root -l -q -b file:${ROOF} an.C ; done` to print hit counts for all simulated threshold settings.
