#!/bin/sh

INSTALL_DIR="/home/tracker/x-ray-tools"

. $INSTALL_DIR/macros/loopOnThresholds_Utils.sh

# loop on all the selected thresholds

## Cu (2023-02-22)
#data_dir=${INSTALL_DIR}/20230222_cu*
#target_file='Cu_50kV_20mA_150mm.dat'
#rm -f ${target_file}
#touch ${target_file}
#
#for THR in {444..473..1}; do 
#    echo $THR >> ${target_file}
#    # get threshold in VCal units ("sed" to remove escape characters from the output string)
#    grep -Fn Average ${data_dir}/threshold${THR}gdac/setuplogs/CMSITminiDAQ000002_scurve.log | sed 's/\x1b//g' >> ${target_file}
#    # compute occupancy
#    python3 ${INSTALL_DIR}/macros/ComputeOccupancyPhysicsRun.py ${data_dir}/threshold${THR}gdac/Results/Run*_Physics.root | grep -E 'OCCUPANCY|WARNING' >> ${target_file}
#    mv Run00000S_Physics.root ${data_dir}/threshold${THR}gdac/
#done

# Cu (2023-02-28)
data_dir=${INSTALL_DIR}/20230228_cu*
target_file='Cu_50kV_20mA_47mm.dat'
rm -f ${target_file}
touch ${target_file}

#for THR in {444..473..1}; do 
#    echo $THR >> ${target_file}
#    # get threshold in VCal units ("sed" to remove escape characters from the output string)
#    grep -Fn Average ${data_dir}/threshold${THR}gdac/setuplogs/CMSITminiDAQ000002_scurve.log | sed 's/\x1b//g' >> ${target_file}
#    # compute occupancy
#    python3 ${INSTALL_DIR}/macros/ComputeOccupancyPhysicsRun.py ${data_dir}/threshold${THR}gdac/Results/Run*_Physics.root | grep -E 'OCCUPANCY|WARNING' >> ${target_file}
#    mv Run00000S_Physics.root ${data_dir}/threshold${THR}gdac/
#done
#analyzethr 444 473

# Mo
data_dir=${INSTALL_DIR}/20230301_mo*
target_file='Mo_50kV_20mA_47mm.dat'
rm -f ${target_file}
touch ${target_file}

echo Analyzing Mo
analyzethr 523 570

## Fe
#data_dir=${INSTALL_DIR}/20230301_fe*
#target_file='Fe_50kV_20mA_47mm.dat'
#rm -f ${target_file}
#touch ${target_file}
#
#for THR in {440..460..1}; do 
#    echo $THR >> ${target_file}
#    # get threshold in VCal units ("sed" to remove escape characters from the output string)
#    grep -Fn Average ${data_dir}/threshold${THR}gdac/setuplogs/CMSITminiDAQ000002_scurve.log | sed 's/\x1b//g' >> ${target_file}
#    # compute occupancy
#    python3 ${INSTALL_DIR}/macros/ComputeOccupancyPhysicsRun.py ${data_dir}/threshold${THR}gdac/Results/Run*_Physics.root | grep -E 'OCCUPANCY|WARNING' >> ${target_file}
#    mv Run00000S_Physics.root ${data_dir}/threshold${THR}gdac/
#done

# Fe (2023-03-02)
data_dir=${INSTALL_DIR}/20230302_fe*
target_file='Fe_50kV_20mA_47mm_nocap.dat'
rm -f ${target_file}
touch ${target_file}

echo Analyzing Fe
analyzethr 440 470

# Ag (2023-03-06)
data_dir=${INSTALL_DIR}/20230306_ag*
target_file='Ag_50kV_20mA_47mm.dat'
rm -f ${target_file}
touch ${target_file}

Analyzing Ag
analyzethr 564 600

python3 ShowSpectrum.py --dvcal --raw
mv Spectrum_RD53B.root Spectrum_RD53B_raw.root
python3 ShowSpectrum.py --dvcal
