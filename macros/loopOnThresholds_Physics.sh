#!/bin/sh

# Simple script to perform physics runs (4x) on a list of thresholds
# Occupancy is computed at the end and written in a text file

INSTALL_DIR="/home/tracker/x-ray-tools-Pagani"

# loop on all the selected thresholds
#for THR in {1400..7500..100}; do 
for THR in {2100..3200..100}; do 

	# cd to the specific folder/threshold
	cd threshold$THR
	pwd

	# clean old files taken in "Physics" mode and optionally reset RunNumber
        find Results  -name Run\*Physics.root -exec rm {} \;
        find logs     -name CMSITminiDAQ\*_physics.log -exec rm {} \;
        rm RunNumber.txt
        # optionally reset run number
        # echo '000005' > RunNumber.txt 
        # optionally save binary data 
        # mv CMSIT.xml CMSIT.xml.old
        # sed "s/SaveBinaryData\"> *0/SaveBinaryData\">1/" CMSIT.xml.old > CMSIT.xml
        # sed "s/nTRIGxEvent\"> *1 *</nTRIGxEvent\">10</" CMSIT.xml.old > CMSIT.xml
	for RUN in {1..4}; do
                echo "LOOPRUNNING ${THR} INSTANCE ${RUN}" 
		# run ph2_acf in physics mode
		CMSITminiDAQ -f CMSIT.xml -c physics -t 30
	done  
        # compute occupancy
	python3 ${INSTALL_DIR}/macros/ComputeOccupancyPhysicsRun.py Results/Run*_Physics.root > Results/occupancy_physics.txt
        echo "LOOPRUNNINGDONE ${THR}"
	# go back to parent folder 
	cd ..
done


