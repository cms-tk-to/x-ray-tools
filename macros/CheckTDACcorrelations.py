# simple script to study the correlation between the TDAC trim bits from two ph2_acf runs
import argparse
import ROOT

TDAC_MAX = 31

# range of CROC columns depending on DAC_GDAC_M_LIN
RANGE_LIN = range(2, 430)

# fcn to return the TDAC bits from CMSIT_RD53.txt
def getTDAC(filename_rd53_txt):
    fp = open(filename_rd53_txt,'r')

    tdac_dict = {}
    # read txt file line by line
    lines = fp.read().splitlines()
    for i in range(len(lines)):
        if lines[i].startswith('COL'):
            # column number
            col = int( lines[i].split()[1] )
            # check if LIN front-end
            if col in RANGE_LIN:
                # the values of the TDAC bits are 4 lines below
                tdac_line = lines[i+4]
                # parse the TDAC line and create a list of the TDAC bits
                _, tdac_values = tdac_line.split()[0], [ int(x) for x in (tdac_line.split()[1]).split(',') ]
                tdac_dict[col] = tdac_values
    fp.close()
    return tdac_dict

def main():
    parser = argparse.ArgumentParser(
        description='Produce the channel-by-channel correlation histogram of '
                    'the TDAC for two chip configurations.')
    parser.add_argument('filename',
                        nargs=2,
                        help='.txt file with chip configuration')
    parser.add_argument('--rd53a',
                        action='store_true',
                        help='RD53A compatibility mode')
    args = parser.parse_args()
    if args.rd53a:
        global TDAC_MAX, RANGE_LIN
        TDAC_MAX = 15
        RANGE_LIN = range(128, 263+1)

    # initialize ROOT stuff
    file_root = ROOT.TFile('TDACCorrelation.root','RECREATE')
    h2 = ROOT.TH2F('TDAC_correlation','TDAC_correlation',
                   TDAC_MAX+1, -0.5, TDAC_MAX+0.5,
                   TDAC_MAX+1, -0.5, TDAC_MAX+0.5)
    h2.Sumw2()

    dict_1 = getTDAC(args.filename[0])
    dict_2 = getTDAC(args.filename[1])

    # transform dict to set as set.intersection() is more efficient to select entries with the same key
    set_1 = set(dict_1)
    set_2 = set(dict_2)
    # loop on common entries
    for col in set_1.intersection(set_2):
        # loop on all the channels in a column
        for i in range(len(dict_1[col])):
            h2.Fill(dict_1[col][i], dict_2[col][i])

    # end of ROOT stuff
    print(h2.GetCorrelationFactor())
    h2.Write()
    file_root.Close()

if __name__ == "__main__":
	main()
