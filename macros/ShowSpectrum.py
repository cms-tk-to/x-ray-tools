import ROOT
import locale
import array
import math
import argparse

INDEX_THR_START = 0
INDEX_MEA_START = 26
INDEX_RMS_START = 36
INDEX_DVCAL_START = 112

ELECTRONS_TO_ENERGY = 3.6
DVCAL_TO_ELECTRONS  = 5.1

def get_derivative(x_array, y_array, yE_array):
    # compute derivative from finite differences (2nd order formula extracted from Lagrange interpolation)
    # https://lcn.people.uic.edu/classes/che205s17/docs/che205s17_reading_01e.pdf
    nn = len(x_array)
    dy_array  = array.array('f',[])
    dyE_array = array.array('f',[])
    for ii, _ in enumerate(x_array):
        # first/last point computed with one-side difference quotient
        if ii == 0:
            dyE = 0 #TODO error propagation
            x1, x2, x3 = [x_array[i] for i in range(ii, ii + 3)]
            f1, f2, f3 = [y_array[i] for i in range(ii, ii + 3)]
            x21, x31, x32 = (x2 - x1), (x3 - x1), (x3 - x2)
            dy = 0
            dy += - (x21 + x31) / (x21 * x31) * f1
            dy += + x31 / (x21 * x32) * f2
            dy += - x21 / (x32 * x31) * f3
        elif ii == nn-1:
            dyE = 0 #TODO error propagation
            x1, x2, x3 = [x_array[i] for i in range(ii - 3, ii - 3 + 3)]
            f1, f2, f3 = [y_array[i] for i in range(ii - 3, ii - 3 + 3)]
            x21, x31, x32 = (x2 - x1), (x3 - x1), (x3 - x2)
            dy = 0
            dy += + x32 / (x31 * x21) * f1
            dy += - x31 / (x21 * x32) * f2
            dy += + (x32 + x31) / (x32 * x31) * f3
        # points in the middle computed with two-sides difference quotient
        else:
            dyE = 0 #TODO error propagation
            x1, x2, x3 = [x_array[i] for i in range(ii - 1, ii - 1 + 3)]
            f1, f2, f3 = [y_array[i] for i in range(ii - 1, ii - 1 + 3)]
            x21, x31, x32 = (x2 - x1), (x3 - x1), (x3 - x2)
            dy = 0
            dy += - x32 / (x31 * x21) * f1
            dy += - (x21 - x32) / (x21 * x32) * f2
            dy += + x21 / (x31 * x32) * f3

        #dy_array.append(y_array[ii])
        #dyE_array.append(yE_array[ii])
        dy_array.append(-dy)
        dyE_array.append(dyE)
    return (dy_array, dyE_array)

def plot_element(dat_file, use_dvcal, title, color, plot_raw):
    thr  = array.array('f',[])
    thrE = array.array('f',[])
    mean = array.array('f',[])
    rms  = array.array('f',[])
    der  = array.array('f',[])
    derE = array.array('f',[])

    # fill arrays
    with open(dat_file, "r") as file_thr:

        for the_line_no, the_line in enumerate(file_thr):
            if the_line_no%3 == 0:
                thr_val   = locale.atof(the_line[INDEX_THR_START:INDEX_THR_START+3])
            elif the_line_no%3 == 1:
                #dvcal = locale.atof(the_line[INDEX_DVCAL_START:INDEX_DVCAL_START+5])
                dvcal = float(the_line)
            elif the_line_no%3 == 2:
                if use_dvcal:
                    thr.append(dvcal)
                else:
                    thr.append(thr_val)
                thrE.append(0.0)
                mean.append(locale.atof(the_line[INDEX_MEA_START:INDEX_MEA_START+9]))
                rms.append(locale.atof(the_line[INDEX_RMS_START:INDEX_RMS_START+9]))
        y, yE = (mean, rms) if plot_raw else get_derivative(thr, mean, rms)

    # plot
    gr = ROOT.TGraphErrors(len(thr), thr, y, thrE, yE)
    gr.SetTitle(title)
    gr.SetName(title)
    gr.SetLineColor(color)
    gr.SetMarkerColor(color)
    gr.SetMarkerStyle(ROOT.kFullCircle)
    gr.SetLineWidth(3)

    return gr

def get_element_lines(input_data_filename):
    # https://xdb.lbl.gov/Section1/Table_1-3.pdf
    kakb_ST_dict = {
        'Ti': ( 4511.,  4932.),
        'Fe': ( 6404.,  7058.),
        'Cu': ( 8048.,  8905.),
        'Mo': (17479., 19608.),
        'Ag': (22163., 24942.),
        'Sn': (25271., 28486.),
    }

    try:
        return kakb_ST_dict[input_data_filename[:2]][0]/(ELECTRONS_TO_ENERGY * DVCAL_TO_ELECTRONS), kakb_ST_dict[input_data_filename[:2]][1]/(ELECTRONS_TO_ENERGY * DVCAL_TO_ELECTRONS)
    except Exception as inst:
        print( 'Secondary Target Not Found' )
        print(type(inst))
        print(inst.args)
        print(inst)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--dvcal", action="store_true", help="use GDAC (default) or DVCal")
    parser.add_argument("--raw", action="store_true", help="show raw counts, no derivative")
    args = parser.parse_args()

    c1 = ROOT.TCanvas("c1","c1",200,10,700,500)
    mg = ROOT.TMultiGraph("mg","RD53B")

    mg.GetXaxis().SetTitle(
        "threshold [#DeltaVCal]" if args.dvcal else "threshold [GDAC]")
    mg.GetYaxis().SetTitle(
        "hits" if args.raw else "#minus (dN/dthr) [a.u.]")

    dataset_list = [
        ("Cu_50kV_20mA_47mm_nocap.dat", "Cu", ROOT.kOrange+5), # alias ("Cu_50kV_20mA_47mm_nocap.dat", "Cu (7 Mar)", ROOT.kOrange+5),
        ("Mo_50kV_20mA_47mm.dat", "Mo", ROOT.kOrange),
        ("Fe_50kV_20mA_47mm_nocap.dat", "Fe", ROOT.kGreen),
	    ("Ag_50kV_20mA_47mm.dat", "Ag", ROOT.kGray + 1),
        ]

    # add plots for all elements
    for dat_file, title, color in dataset_list:
        gr = plot_element(dat_file, args.dvcal, title, color, args.raw)
        mg.Add(gr)
    # draw
    _ = mg.Draw("ap") if args.raw else mg.Draw("apl")
    
    c1.SetGridx()
    c1.SetGridy()
    c1.BuildLegend()

    # update the canvas to force the new values of the coordinates (before drawing the lines)
    c1.Update()
    ymin = c1.GetUymin()
    ymax = c1.GetUymax()*0.75
    line_ka_list = []
    line_kb_list = []
    for dat_file, title, color in dataset_list:
        # draw reference Ka and Kb lines (for the moment only when using DVCal units)
        if args.dvcal:
            x_ka, x_kb = get_element_lines(dat_file)

            line = ROOT.TLine(x_ka, ymin, x_ka, ymax);
            line.SetLineColor(color)
            line.SetLineWidth(2)
            line.SetLineStyle(ROOT.kDashDotted)
            line_ka_list.append(line)
            line_ka_list[-1].Draw()

            line = ROOT.TLine(x_kb, ymin, x_kb, ymax);
            line.SetLineColor(color)
            line.SetLineWidth(2)
            line.SetLineStyle(ROOT.kDashDotted)
            line_kb_list.append(line)
            line_kb_list[-1].Draw()

    c1.SaveAs("Spectrum_RD53B_raw.root" if args.raw else "Spectrum_RD53B.root")

##################################
if __name__ == "__main__":
    main()
