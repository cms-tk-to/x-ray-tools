# script to add the Occupancy 2D histo produced by Ph2_ACF run in "physics" mode
# usage: python ComputeOccupancyPhysicsRun.py [list of input root files, including wildcards]
import itertools
import math
import sys
import ROOT
import re


DEBUG = True # False

# ChipID = '0'
# RANGE_COL_LIN = range(128, 263+1)
# RANGE_ROW_LIN = range(  0, 191+1)

ChipID = '15'
#following coordinates using TH2 conventions
#RANGE_COL_LIN = range(1, 432+1)
#RANGE_ROW_LIN = range(1, 336+1)
#RANGE_COL_LIN = range(100, 300+1)
#RANGE_ROW_LIN = range( 18, 318+1)
RANGE_COL_LIN = range(150+1, 300+1)
RANGE_ROW_LIN = range(80+1, 240+1)

def roi():
	col_c, row_c = 237, 159 #center of ROI
	rad = 80 #radius of ROI
	r2 = rad ** 2
	gdac_m_region = itertools.product(range(336), range(2, 430))
	cond = lambda r_c: (r_c[0] - row_c) ** 2 + (r_c[1] - col_c) ** 2 < r2
	return filter(cond, gdac_m_region)

def compute_mean_stddev(the_list):
# calculate sample mean and standard deviation of list
    val_sum = 0
    w_sum = 0
    for val, err in the_list:
        w = 1 / (err * err)
        w_sum += w
        val_sum += val * w
    return val_sum / w_sum, 1 / math.sqrt(w_sum)

def main():
    ROOT.gROOT.SetBatch(True)

    list_of_files = sys.argv[1:]    
    list_of_occupancy = [0.0] * len(list_of_files)
    list_of_occ_err = [0.0] * len(list_of_files)
    n_bx = 0
    regex_bx = re.compile(r"received triggers.*\D(\d+)[^m\d]")
# TODO add switch to steer between old/new version of ph2_acf
#    regex_bx = re.compile(r"recorded events.*\D(\d+)[^m\d]")
    
    for index, root_file_name in enumerate(list_of_files):
        if DEBUG:
            print(f'>>> {root_file_name}')
            cnt_tmp = 0
        # open input root files
        root_file = ROOT.TFile(root_file_name,'READ')

        # 2D OCC
        c = root_file.Get('/Detector/Board_0/OpticalGroup_0/Hybrid_0/Chip_'+ChipID+'/D_B(0)_O(0)_H(0)_Occ2D_Chip('+ChipID+')')
        for p in c.GetListOfPrimitives():
            if p.InheritsFrom('TH2') and p.GetName() == 'D_B(0)_O(0)_H(0)_Occ2D_Chip('+ChipID+')':
                h2_tmp = c.GetPrimitive('D_B(0)_O(0)_H(0)_Occ2D_Chip('+ChipID+')')
                if DEBUG:
                    for row, col in roi():
                            cnt_tmp += h2_tmp.GetBinContent(col, row) 
                    print(f'COUNTS {cnt_tmp}')

                if index == 0:
                    h2_occ = h2_tmp.Clone(h2_tmp.GetName()+'_new')
                    # based on https://root-forum.cern.ch/t/setdirectory-0-using-pyroot-inside-a-function/41700
                    # remove the ownership of the cloned th1 (otherwise the ownership is linked to root file currently opened, and it will deleted when the file will be closed)
                    h2_occ.SetDirectory(0)
                else:
                    h2_occ.Add(h2_tmp)

#                if DEBUG:
#                    print( 'OCC 2D', h2_occ.GetEntries() )
        root_file.Close()
        
        log_file_name = root_file_name.replace('Results/Run', 'logs/CMSITminiDAQ').replace('Physics.root', 'physics.log')
        with open(log_file_name) as log_file:
            for line in log_file.readlines():
                match = regex_bx.search(line)
                if match is not None:
                    n_bx += int(match.group(1))
                    n_bx_tmp = int(match.group(1))
                    break
        print(f'COUNTS {cnt_tmp} NBX {n_bx_tmp}') #, float(cnt_tmp) / float( 192*(264-128) ) / n_bx_tmp
        norm = len(list(roi())) * n_bx_tmp
        list_of_occupancy[index] = float(cnt_tmp) / norm
        list_of_occ_err[index] = math.sqrt(cnt_tmp) / norm

    occ_mean, occ_stddev = compute_mean_stddev(zip(list_of_occupancy, list_of_occ_err))
    print(f'OCCUPANCY (mean, stddev): {occ_mean:.3e} {occ_stddev:.3e}')
    if occ_stddev > occ_mean/10.:
        print(f'*** WARNING! OCCUPANCIES NOT CONSISTENT (mean, stddev): {occ_mean:.3e} {occ_stddev:.3e}')
        for index, _ in enumerate(list_of_files):
            occ_tmp = list_of_occupancy[index]
            print(f' *** Occupancy {occ_tmp:.3e} {list_of_files[index]}') 

    root_output_file = ROOT.TFile('Run00000S_Physics.root','RECREATE')
    n_bin = 200
    h1_occ_min = -0.5 / n_bx
    h1_occ_max = 199.5 / n_bx
    h1_occ = ROOT.TH1D('h1_occ', 'Single-channel occupancy distribution', n_bin, h1_occ_min, h1_occ_max)
    h2_roi = ROOT.TH2D('h2_roi', 'Single-channel occupancy;Column;Row', 432, 0, 432, 336, 0, 336)
    cnt = 0
    tol = 0.999999
    for row, col in roi():
            cnt += h2_occ.GetBinContent(col, row) 
            occ = h2_occ.GetBinContent(col, row) / float(n_bx)
            h2_roi.SetBinContent(col, row, occ)
            occ = min(tol*h1_occ_max, occ)
            occ = max(h1_occ_min, occ)
            h1_occ.Fill(occ)
    # simple attemp to intercept unmasked noisy channels 
    if h1_occ.GetBinContent(n_bin) > 0:
        print('ComputeOccupancyPhysicsRun WARNING: Noisy Channels', file=sys.stderr)

    h2_occ.Write()
    h2_roi.Write()
    h1_occ.Write()
    root_output_file.Close()

##################################
if __name__ == "__main__":
    main()
