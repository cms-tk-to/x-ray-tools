import itertools

def roi():
	col_c, row_c = 237, 159 #center of ROI
	rad = 80 #radius of ROI
	r2 = rad ** 2
	gdac_m_region = itertools.product(range(336), range(2, 430))
	cond = lambda r_c: (r_c[0] - row_c) ** 2 + (r_c[1] - col_c) ** 2 < r2
	return filter(cond, gdac_m_region)

chip_id = 15

################################################################################
# You don't need to edit the code past this point to use this program. #########
################################################################################

def main():
	import argparse

	import ROOT

	parser = argparse.ArgumentParser(description='Prints to standard output '
	                                             'the average threshold '
	                                             'for the preset ROI.')
	parser.add_argument('filename',
	                    help='ROOT file with the scurve calibration results')

	args = parser.parse_args()

	f = ROOT.TFile(args.filename)

	if not f.IsOpen():
		raise RuntimeError('File not found!')

	c = f.Get(f'Detector/Board_0/OpticalGroup_0/Hybrid_0/'
	          f'Chip_{chip_id}/'
	          f'D_B(0)_O(0)_H(0)_Threshold2D_Chip({chip_id})')

	if not isinstance(c, ROOT.TCanvas):
		raise RuntimeError('Expected canvas not found in file!')

	h = c.GetPrimitive(f'D_B(0)_O(0)_H(0)_Threshold2D_Chip({chip_id})')

	if not isinstance(h, ROOT.TH2):
		raise RuntimeError('Expected histogram not found in file!')

	n_thr = 0
	sum_thr = 0
	for x, y in roi():
		thr = h.GetBinContent(x, y)
		if thr == 0:
			continue #skips masked channels
		sum_thr += thr
		n_thr += 1

	print(sum_thr / n_thr)

if __name__ == '__main__':
	main()
