#!/bin/sh

INSTALL_DIR="/home/tracker/x-ray-tools"

. $INSTALL_DIR/macros/loopOnThresholds_Utils.sh

# loop on all the selected thresholds

# Cu (2023-03-07) (no cap)
data_dir=${INSTALL_DIR}/20230307_cu*
target_file='Cu_50kV_20mA_47mm_nocap.dat'
rm -f ${target_file}
touch ${target_file}
analyzethr 444 480

python3 ShowSpectrum.py --dvcal --raw
mv Spectrum_RD53B.root Spectrum_RD53B_raw.root
python3 ShowSpectrum.py --dvcal
