import argparse
import array
import itertools
import math

import ROOT

import Ph2_ACF_utils

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('--txt', required=True,
	                    help='Path to .txt configuration file')
	parser.add_argument('-i', required=True,
	                    help='Path to ROOT file with physics data')
	parser.add_argument('-o', required=True,
	                    help='Destination ROOT file with results')
	args = parser.parse_args()

	with open(args.txt) as txt_file:
		en, inj = Ph2_ACF_utils.get_en_and_inj(txt_file)
	pixels4rate, pixels4eff = Ph2_ACF_utils.get_pixels_for_rate_and_efficiency(en, inj)

	#TODO determine n_bx at runtime
	n_bx = 40_000

	#opening the output file first, so that it owns the tree
	out_file = ROOT.TFile(args.o, 'RECREATE')
	tree = ROOT.TTree('tree', 'High-rate efficiency data')
	mrow_tree = array.array('I', [0])
	mcol_tree = array.array('I', [0])
	rate_tree = array.array('f', [0.0])
	eff_tree = array.array('f', [0.0])
	rate_w_corr_tree = array.array('f', [0.0])
	rate_err_tree = array.array('f', [0.0])
	eff_err_tree = array.array('f', [0.0])
	rate_w_corr_err_tree = array.array('f', [0.0])
	rate_asym_x_tree = array.array('f', [0.0])
	rate_asym_y_tree = array.array('f', [0.0])
	rate_sigma_tree = array.array('f', [0.0])
	tot_tree = array.array('f', [0.0])
	tree.Branch('mrow', mrow_tree, 'mrow/i')
	tree.Branch('mcol', mcol_tree, 'mcol/i')
	tree.Branch('rate', rate_tree, 'rate/F')
	tree.Branch('eff', eff_tree, 'eff/F')
	tree.Branch('rate_w_corr', rate_w_corr_tree, 'rate_w_corr/F')
	tree.Branch('rate_err', rate_err_tree, 'rate_err/F')
	tree.Branch('eff_err', eff_err_tree, 'eff_err/F')
	tree.Branch('rate_w_corr_err', rate_w_corr_err_tree, 'rate_w_corr_err/F')
	tree.Branch('rate_asym_x', rate_asym_x_tree, 'rate_asym_x/F')
	tree.Branch('rate_asym_y', rate_asym_y_tree, 'rate_asym_y/F')
	tree.Branch('rate_sigma', rate_sigma_tree, 'rate_sigma/F')
	tree.Branch('tot', tot_tree, 'tot/F')

	in_file = ROOT.TFile(args.i)
	occ_histo = in_file.Get('/Detector/Board_0/OpticalGroup_0/Hybrid_0/'
	                        'Chip_15/D_B(0)_O(0)_H(0)_Occ2D_Chip(15)') \
	                   .GetPrimitive('D_B(0)_O(0)_H(0)_Occ2D_Chip(15)')
	tot_histo = in_file.Get('/Detector/Board_0/OpticalGroup_0/Hybrid_0/'
	                        'Chip_15/D_B(0)_O(0)_H(0)_ToT2D_Chip(15)') \
	                   .GetPrimitive('D_B(0)_O(0)_H(0)_ToT2D_Chip(15)')

	BLOCK_LEN = 8
	#loop over all pixel blocks
	histo_occ_core = ROOT.TH1I('histo_occ_core', '', 100, 0, 100)
	for mrow, mcol in itertools.product(range(336 // BLOCK_LEN), range(432 // BLOCK_LEN)):
		histo_occ_core.Reset()

		hits4rate = 0 #counts hits in pixels for rate estimation
		hits4eff = 0 #counts hits in pixels for efficiency estimation
		n_rate = 0 #no. of pixels for rate estimation
		n_eff = 0 #no. of pixels for efficiency estimation
		tot_tot = 0.0 #sum of avg. ToT's across injected pixels
		x1, y1 = 0.0, 0.0
		x2, y2 = 0.0, 0.0
		x3, y3 = 0.0, 0.0

		#loop over all pixels in the same block
		for i, j in itertools.product(range(BLOCK_LEN), range(BLOCK_LEN)):
			row, col = (mrow * BLOCK_LEN + i), (mcol * BLOCK_LEN + j)
			occ = occ_histo.GetBinContent(col + 1, row + 1)
			tot = tot_histo.GetBinContent(col + 1, row + 1)

			if (row, col) in pixels4rate:
				n_rate += 1
				hits4rate += occ
				x = col - (BLOCK_LEN - 1) / 2
				y = row - (BLOCK_LEN - 1) / 2
				x1 += occ * (x ** 1)
				x2 += occ * (x ** 2)
				x3 += occ * (x ** 3)
				y1 += occ * (y ** 1)
				y2 += occ * (y ** 2)
				y3 += occ * (y ** 3)
				histo_occ_core.Fill(occ)

			elif (row, col) in pixels4eff:
				n_eff += 1
				hits4eff += occ
				tot_tot += tot

		mrow_tree[0], mcol_tree[0] = mrow, mcol

		#rate measures
		try:
			norm = 1.0;
			norm *= n_rate * 0.005 ** 2 #total area [cm2]
			norm *= n_bx * 25 #total sampling time [ns]
			#rate is thus expressed in units of GHz/cm2

			rate_tree[0] = hits4rate / norm
			rate_err_tree[0] = math.sqrt(hits4rate) / norm

			#moments computation
			x1 /= hits4rate
			x2 /= hits4rate
			x3 /= hits4rate
			y1 /= hits4rate
			y2 /= hits4rate
			y3 /= hits4rate
			x2_c = x2 - x1 ** 2
			x3_c = x3 + x1 * (2 * x2_c - x2)
			y2_c = y2 - y1 ** 2
			y3_c = y3 + y1 * (2 * y2_c - y2)

			rate_asym_x_tree[0] = math.copysign(
				math.fabs(x3_c) ** (1/3) / math.sqrt(x2_c),
				x3_c)
			rate_asym_y_tree[0] = math.copysign(
				math.fabs(y3_c) ** (1/3) / math.sqrt(y2_c),
				y3_c)

			rate_sigma_tree[0] = histo_occ_core.GetStdDev()
			rate_sigma_tree[0] *= math.sqrt(n_rate / norm)
		except ZeroDivisionError:
			rate_tree[0] = -1.0
			rate_err_tree[0] = -1.0
			rate_asym_x_tree[0] = 0.0
			rate_asym_y_tree[0] = 0.0
			rate_sigma_tree[0] = -1.0

		#efficiency measures
		try:
			eff = hits4eff / (n_eff * n_bx)
			tot_tree[0] = tot_tot / n_eff
		except:
			eff = -1.0
			tot_tree[0] = -1.0
		try:
			eff_err = math.sqrt(eff * (1 - eff) / (n_eff * n_bx))
		except:
			eff_err = -1.0
		eff_tree[0] = eff
		eff_err_tree[0] = eff_err

		#efficiency-corrected rate
		if eff_tree[0] > 0.0 and rate_tree[0] != -1.0:
			rate_w_corr_tree[0] = rate_tree[0] / eff_tree[0]
			rate_w_corr_err_tree[0] = (
				rate_w_corr_tree[0] * math.sqrt(
					(rate_err_tree[0] / rate_tree[0]) ** 2
					+ (eff_err_tree[0] / eff_tree[0]) ** 2
				)
			)
		else:
			rate_w_corr_tree[0] = -1.0
			rate_w_corr_err_tree[0] = -1.0

		tree.Fill()

	out_file.Write()

if __name__ == '__main__':
	main()
