#!/bin/sh

analyzethr () {
    THR=$1
    THRSTEP="${3:-1}"
    while [ $THR -le $2 ]
    do
        echo "Analyzing GDAC $THR"
        echo $THR >> ${target_file}
        # get threshold in VCal units ("sed" to remove escape characters from the output string)
        ###grep -Fn Average ${data_dir}/threshold${THR}gdac/setuplogs/CMSITminiDAQ000002_scurve.log | sed 's/\x1b//g' >> ${target_file}
        python3 ${INSTALL_DIR}/macros/ComputeThresholdSCurve.py ${data_dir}/threshold${THR}gdac/setupresults/Run*_SCurve.root >> ${target_file}
        # compute occupancy
        python3 ${INSTALL_DIR}/macros/ComputeOccupancyPhysicsRun.py ${data_dir}/threshold${THR}gdac/Results/Run*_Physics.root | grep -E 'OCCUPANCY|WARNING' >> ${target_file}
        mv Run00000S_Physics.root ${data_dir}/threshold${THR}gdac/
        THR=$((THR+THRSTEP))
    done
}
