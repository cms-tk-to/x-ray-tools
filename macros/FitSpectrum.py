import math
import ROOT
import array

'''
def func_f1(x, p):
    tmp_lin = 0.0
    if x[0] < p[1]:
        tmp_lin = p[4]*(x[0]-p[1])
    return p[0] * 0.5*math.erfc((x[0]-p[1])/p[2]) + p[3] + tmp_lin

def func_f2(x, p):
    tmp_lin = 0.0
    if x[0] < p[1]:
        tmp_lin = p[7]*(x[0]-p[1])
    return p[0] * 0.5*math.erfc((x[0]-p[1])/p[2]) + p[3] * 0.5*math.erfc((x[0]-p[4])/p[5]) + p[6] + tmp_lin
'''

def func_f1(x, p):
    from math import erfc, exp, pi, sqrt
    mu = p[1]
    d = x[0] - mu
    width = p[2] # == sigma * sqrt(2)
    a = p[0]
    b = p[4]
    return (a + b*d) / 2 * erfc(d / width) - b*width / (2*sqrt(pi)) * exp(-(d/width)**2) + p[3]

def func_f2(x, p):
    from math import erfc, exp, pi, sqrt
    mu1 = p[1]
    d1 = x[0] - mu1
    width1 = p[2] # == sigma * sqrt(2)
    a1 = p[0]
    b1 = p[7]
    mu2 = p[4]
    d2 = x[0] - mu2
    width2 = p[5]
    a2 = p[3]
    b2 = p[7] * a2/a1 * mu1/mu2
    return   (a1 + b1*d1) / 2 * erfc(d1 / width1) - b1*width1 / (2*sqrt(pi)) * exp(-(d1/width1)**2) \
           + (a2 + b2*d2) / 2 * erfc(d2 / width2) - b2*width2 / (2*sqrt(pi)) * exp(-(d2/width2)**2) \
           + p[6]

DVCAL_TO_ELECTRONS  = 5.1
ELECTRONS_TO_ENERGY = 3.6
kakb_ST_dict = {
    'Ti': ( 4511.,  4932.),
    'Fe': ( 6404.,  7058.),
    'Cu': ( 8048.,  8905.),
    'Mo': (17479., 19608.),
    'Ag': (22163., 24942.),
    'Sn': (25271., 28486.),
}

# define TF1s for the fit
paramFe = array.array('d', [2e-6, 300., 50., 1e-8, -1e-9])
paramCu = array.array('d', [2e-6, 400., 50., 1e-8, -1e-9])

paramMo = array.array('d', [0.8e-6,  900., 50., 2e-7, 1.1e3, 50., 1e-9, -1e-10])
paramAg = array.array('d', [0.2e-6, 1200., 50., 5e-8, 1.3e3, 50., 1e-9, -1e-10])

dict_func = {}

dict_func['Fe'] = ROOT.TF1('fit_Fe', func_f1, 250., 500., 5)
dict_func['Fe'].SetParameters(paramFe)
dict_func['Fe'].SetParName(1,'Kalpha')
dict_func['Fe'].SetParName(2,'Salpha')
dict_func['Fe'].SetNpx(50000)

dict_func['Cu'] = ROOT.TF1('fit_Cu', func_f1, 250., 600., 5)
dict_func['Cu'].SetParameters(paramCu)
dict_func['Cu'].SetParName(1,'Kalpha')
dict_func['Cu'].SetParName(2,'Salpha')
dict_func['Cu'].SetNpx(50000)

dict_func['Mo'] = ROOT.TF1('fit_Mo', func_f2, 800., 1300., 8)
dict_func['Mo'].SetParameters(paramMo)
dict_func['Mo'].SetParName(1,'Kalpha')
dict_func['Mo'].SetParName(2,'Salpha')
dict_func['Mo'].SetParName(4,'Kbeta')
dict_func['Mo'].SetParName(5,'Sbeta')
dict_func['Mo'].SetNpx(50000)

dict_func['Ag'] = ROOT.TF1('fit_Ag', func_f2, 1100., 1500., 8)
dict_func['Ag'].SetParameters(paramAg)
dict_func['Ag'].SetParName(1,'Kalpha')
dict_func['Ag'].SetParName(2,'Salpha')
dict_func['Ag'].SetParName(4,'Kbeta')
dict_func['Ag'].SetParName(5,'Sbeta')
dict_func['Ag'].SetNpx(50000)


# fetch the input TGraphErrors
f = ROOT.TFile("Spectrum_RD53B_raw.root","READ")
c1 = f.Get("c1")
mg = c1.GetPrimitive("mg")
l = mg.GetListOfGraphs()

#
# declare arrays for final regression (DVCal vs electrons)
xx = array.array('d',[])
yy = array.array('d',[])
exx = array.array('d',[])
eyy = array.array('d',[])

mg_res = ROOT.TMultiGraph("mg_res","RD53B")
mg.GetXaxis().SetTitle("threshold [#DeltaVCal]")
mg.GetYaxis().SetTitle("hits" )

ERROR_SCALE_FACTOR = 40.
for gr in sorted(l):

        name = gr.GetName()
        # skip if no model was defined for this curve
        try:
            dict_func[name]
        except:
            continue
        print('==> ', name)
        gr.Fit(dict_func[name])

        xx.append(dict_func[name].GetParameter(1))
        exx.append(dict_func[name].GetParError(1))
        yy.append(kakb_ST_dict[name][0]/ELECTRONS_TO_ENERGY)
        print("Kalpha (expected) [DVCal]: ", yy[-1]/DVCAL_TO_ELECTRONS)
        eyy.append(0)

        if dict_func[name].GetNpar() == 8:
            xx.append(dict_func[name].GetParameter(4))
            exx.append(dict_func[name].GetParError(4))
            yy.append(kakb_ST_dict[name][1]/ELECTRONS_TO_ENERGY)
            print("Kbeta (expected) [DVCal]: ", yy[-1]/DVCAL_TO_ELECTRONS)
            eyy.append(0)

        # TGraph with residual/error
        gr_res = gr.Clone('fitres_'+name)
        gr_res.GetFunction('fit_'+name).Delete()
        for ii in range( gr_res.GetN() ):
            xval = gr_res.GetPointX(ii)
            yval = (dict_func[name].Eval(xval) - gr_res.GetPointY(ii)) / gr_res.GetErrorY(ii)
            gr_res.SetPointY(ii, yval)
        mg_res.Add(gr_res)

cOut = ROOT.TCanvas()
cOut.Divide(1,3)

cOut.cd(1)
mg.Draw("ap")

cOut.cd(2)
mg_res.Draw("ap")

cOut.cd(3)
gre_summary = ROOT.TGraphErrors(6, xx, yy, array.array('d', [exx_tmp*ERROR_SCALE_FACTOR for exx_tmp in exx]), eyy)
gre_summary.Fit('pol1')
gre_summary.Draw('AP')

cOut.SaveAs('cOut.root')
