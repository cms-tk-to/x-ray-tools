# script to extract average Occupancy from 1D histo produced by Ph2_ACF run in "noise" mode
# usage: python ComputeOccupancyNoiseRun.py -f [list of input root files, including wildcards]

from optparse import OptionParser
import array
import ROOT

ChipID = '15'
TO_RATE_GHZCM2 = 0.040/(0.0050*0.0050)

def getTH1GausFit(h1_in, n_sigma, verbose):
########################################

# fit with gaussian (two-steps) and return mu and sigma
    xmin = h1_in.GetXaxis().GetXmin()
    xmax = h1_in.GetXaxis().GetXmax()

# Start with a fit on +-1 RMS
    minfit = max(h1_in.GetMean() - h1_in.GetRMS(),xmin)
    maxfit = min(h1_in.GetMean() + h1_in.GetRMS(),xmax)

    g1 = ROOT.TF1("fcn_gaus","gaus",minfit,maxfit)
    g1.SetLineColor(ROOT.kRed)
    g1.SetLineWidth(2)
    g1.SetRange(minfit,maxfit)
    h1_in.Fit(g1,"RQ")

    # One more iteration
    minfit = max(g1.GetParameter("Mean") - n_sigma*g1.GetParameter("Sigma"),xmin)
    maxfit = min(g1.GetParameter("Mean") + n_sigma*g1.GetParameter("Sigma"),xmax)
    g1.SetRange(minfit,maxfit)
    fit_result_ptr = h1_in.Fit(g1,"RQLS")
    chi2 = 2 * fit_result_ptr.MinFcnValue()
    ndof = g1.GetNDF()

    print('prob(chi2): ', chi2, ROOT.TMath.Prob(chi2, ndof))

    mu = g1.GetParameter("Mean")
    mu_err = g1.GetParError(1)
    sigma = g1.GetParameter("Sigma")
    sigma_err = g1.GetParError(2)

    if verbose:
        print("mu: {:e}".format(mu), "{:e}".format(mu_err))
        print("sigma: {:e}".format(sigma), "{:e}".format(sigma_err))
    print("rate [GHz/cm2]: {:e}".format(mu*TO_RATE_GHZCM2))

    return mu, mu_err, sigma, sigma_err

def main():
########################################
    parser = OptionParser(usage="usage: %prog [options] filename",
                         version="%prog 1.0")
    parser.add_option("-f", "--fileroot",
                    type="string",
                    action="store",
                    dest="ROOTFILENAME",
                    help="ROOT input file")
    parser.add_option("-v", "--verbose",
                    action="store_true",
                    dest="VERBOSE",
                    help="verbose",
                    default=False)

    (options, args) = parser.parse_args()

    ROOT.gROOT.SetBatch(True)

    # input root file
    try:
            root_file = ROOT.TFile.Open(options.ROOTFILENAME,'READ')
            if options.VERBOSE:
                print(f'>>> {options.ROOTFILENAME}')
    except:
            print("No input file specified")
            sys.exit()


    xx     = array.array('d', [0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5])
    xx_err = array.array('d', len(xx)*[0.0])
    yy_mu     = array.array('d', len(xx)*[0.0])
    yy_mu_err = array.array('d', len(xx)*[0.0])
    yy_sigma     = array.array('d', len(xx)*[0.0])
    yy_sigma_err = array.array('d', len(xx)*[0.0])

    c = root_file.Get('/Detector/Board_0/OpticalGroup_0/Hybrid_0/Chip_'+ChipID+'/D_B(0)_O(0)_H(0)_Occ1D_Chip('+ChipID+')')
	# iterate on all the objetcs in the ROOT file and pick up only the relevant TH1
    for p in c.GetListOfPrimitives():
        if p.InheritsFrom('TH1') and p.GetName() == 'D_B(0)_O(0)_H(0)_Occ1D_Chip('+ChipID+')':
            h1_tmp = c.GetPrimitive('D_B(0)_O(0)_H(0)_Occ1D_Chip('+ChipID+')')
            for ii, ss in enumerate(xx):
                yy_mu[ii], yy_mu_err[ii], yy_sigma[ii], yy_sigma_err[ii] = getTH1GausFit(h1_tmp, ss, options.VERBOSE)

    gre_mu    = ROOT.TGraphErrors(len(xx), xx, yy_mu, xx_err, yy_mu_err)
    gre_sigma = ROOT.TGraphErrors(len(xx), xx, yy_sigma, xx_err, yy_sigma_err)
    c1 = ROOT.TCanvas('c1','c1',600,600)
    c1.Divide(1,2)
    c1.cd(1)
    gre_mu.Draw('APE')
    c1.cd(2)
    gre_sigma.Draw('APE')
    c1.SaveAs('c1.root')

##################################
if __name__ == "__main__":
    main()
