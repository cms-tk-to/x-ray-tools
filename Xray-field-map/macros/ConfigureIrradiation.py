#-----------------------------------------------------------------------------------------
# Authors: Beatrice Accotto, Fabio Luongo
# UniTO and INFN Sezione di Torino
#
# 2024.05.20
#-----------------------------------------------------------------------------------------
# Interface application to calculate current of the tube or time of exposure for a user-defined TID dose with the DFP X-ray spectrometer at INFN Torino
# Inputs:
#   size of square area to be irradiated (1x1 mm2 or 2x2 mm2)
#   DeltaZ: distance between edge of the housing and DUT
#   Target dose (in Mrd or kGy)
#   Start time
#   end-time or current of the X-ray tube
#-----------------------------------------------------------------------------------------

import tkinter as tk
import tkinter.ttk
import datetime
import time
import math

KGY_TO_MRD = 0.1
SECS_TO_HOURS = 1./3600.

#------------Function to calculate dose-rate from Z position-----------#
def dose_rate_function(Z):
    global choice_size
    global var_dose_err
    var_dose_err.set(dose_err_function(Z))
    # best fit function in (-1,1) mm from the red laser reference cross; size 1x1 mm2
    if choice_size.get()==1:
        p0=4.83548e+01
        p1=6.53958e+01 # [mm]
        p2=8.64080e-01 # power law exponent 1/pow(x,2+p2)
        err_p0=1.05211e+01
        err_p1=1.29736e+01
        err_p2=3.29345e-01
        return math.pow(p0/(Z+p1),(2+p2))

    # best fit function in (-1,1) mm from the red laser reference cross; size 2x2 mm2
    if choice_size.get()==2:
        p0=4.43165e+01
        p1=6.07916e+01 # [mm]
        p2=7.39936e-01 # power law exponent 1/pow(x,2+p2)
        err_p0=1.07639e+01
        err_p1=1.37058e+01
        err_p2=3.37283e-01
        return math.pow(p0/(Z+p1),(2+p2))

    """
    # best fit function in (-1,2) mm from the red laser reference cross; size 1x1 mm2
    # the position (-1, 2) mm is not the reference anymore
    p0=1.57307e+04
    p1=5.40348e+01 # [mm]
    p2=6.19542e-01 # power law exponent 1/pow(x,2+p2)
    err_p0=2.45114e+04
    err_p1=1.06562e+01
    err_p2=2.75724e-1
    return p0/((Z+p1)**(2+p2))
    """

#----------------Function to determinate dose error------------------#
# dose error set to 5% (fixed) - to be improved
def dose_err_function(Z):
    c=float(Z)*0+5
    return "Dose error= "+str(c)+" %"

#---------------Functions to calculate end-time and current----------#
def t_end_function(Z, dose, t_start, current):
    global choice_dose_unit
    # conversion kGy in Mrd
    if choice_dose_unit.get()==2:
        dose *= KGY_TO_MRD
    t_start=datetime.datetime.fromisoformat(t_start)
    t_end=datetime.timedelta(seconds=dose/current/(dose_rate_function(Z)*SECS_TO_HOURS))+t_start
    return t_end.isoformat()

def current_function(Z, dose, t_start,t_end):
    global choice_dose_unit
    # conversion kGy in Mrd
    if choice_dose_unit.get()==2:
        dose *= KGY_TO_MRD
    t_start=datetime.datetime.fromisoformat(t_start)
    t_start=t_start.timestamp()
    t_end=datetime.datetime.fromisoformat(t_end)
    t_end=t_end.timestamp()
    current=dose/(t_end-t_start)/(dose_rate_function(Z)*SECS_TO_HOURS)
    return current

#-----------This function is activated by the "Calculate" button-----------#
def calculate(*args):

    dose_rate.set("Dose rate= "+str(round(dose_rate_function(float(Z.get())),4))+" Mrd/h/mA")
    dose_rate_label.pack(side=tk.TOP)

    global t_end
    if choice_current_or_end_time.get()==1: # end-time fixed
        var_current.set(current_function(float(Z.get()),float (dose.get()), t_start.get(), t_end.get()))
        pass
    else: #end fixed current case
        var_t_end.set(t_end_function(float(Z.get()), float (dose.get()), t_start.get(), float(current.get())))
        pass

#-----------Function to set Dose title in Mrd or Gy-------------------#
def dose_title_function(*arg):
    global title_dose
    if choice_dose_unit.get()==1:
        title_dose.set("Dose [Mrd]")
    else:
        title_dose.set("Dose [kGy]")


#-----------Trace function linked to endtime vs current radiobutton--------------------#
def current_or_end_time_fixed_function(*args):
    global t_end
    global current
    if choice_current_or_end_time.get()==1: # end-time fixed
        current.config(state='disabled')
        t_end.config(state='normal')
    else:                                    # current of the tube fixed
        t_end.config(state='disabled')
        current.config(state='normal')

#-------------User interface definition --------------------------------------------#
# Root window dimension and name
root=tk.Tk()
root.title("Configure Irradiation")
root.geometry("650x650")
root.resizable(True, True)

label_xy_coordinate=tk.Label(text='DUT in x=-1 mm, y=1 mm from red laser cross - Vtube=40 kV', bg='#f00')
label_xy_coordinate.pack(side=tk.TOP)

# horizontal separator
Separator=tk.StringVar()
Separator="---------------------"
tk.Label(text=Separator).pack(side=tk.TOP)

# variable defining size (1 mm or 2 mm) and related Radiobutton
choice_size=tk.IntVar()
choice_size.set(1)

size_1mm=tk.Radiobutton(root, text='size 1x1 mm2', variable=choice_size, value=1)
size_1mm.pack(side=tk.TOP)
size_2mm=tk.Radiobutton(root, text='size 2x2 mm2', variable=choice_size, value=2)
size_2mm.pack(side=tk.TOP)

tk.Label(text=Separator).pack(side=tk.TOP)

# Z label and Z input field
label_Z=tk.Label(text="\u2206Z [mm] (tube housing edge to DUT distance):")
Z=tk.Entry(root, font="Arial")
label_Z.pack(side=tk.TOP)
Z.pack(side=tk.TOP)

tk.Label(text=Separator).pack(side=tk.TOP)

title_dose=tk.StringVar()

# variable defining Mrd or kGy selection and related RadioButton
choice_dose_unit=tk.IntVar()
choice_dose_unit.trace_add('write', dose_title_function)
choice_dose_unit.set(1)

choice_Mrd=tk.Radiobutton(root, text="Dose [Mrd]", variable=choice_dose_unit, value=1)
choice_Mrd.pack(side=tk.TOP)
choice_kGy=tk.Radiobutton(root, text="Dose [kGy]", variable=choice_dose_unit, value=2)
choice_kGy.pack(side=tk.TOP)

tk.Label(text=Separator).pack(side=tk.TOP)

# Dose label and Dose input field
label_dose=tk.Label(textvariable=title_dose)
dose=tk.Entry(root, font="Arial")
label_dose.pack(side=tk.TOP)
dose.pack(side=tk.TOP)

# Dose error variable and label
var_dose_err=tk.StringVar()
label_dose_error=tk.Label(textvariable=var_dose_err)
label_dose_error.pack(side=tk.TOP)

# t_start label and t_start input field
label_t_start=tk.Label(text="Begin of exposure (YYYY-mm-dd hh:MM:ss)")
var_t_start=tk.StringVar()
t_start=tk.Entry(root, font="Arial", textvariable=var_t_start)
var_t_start.set(datetime.datetime.now())
label_t_start.pack(side=tk.TOP)
t_start.pack(side=tk.TOP)

# tube current variable and tube current input field
var_current=tk.DoubleVar()
label_current=tk.Label(text="Tube current [mA]")
current=tk.Entry(root, font="Arial", textvariable=var_current)

# t_end variable and t_end input field
var_t_end=tk.StringVar()
label_t_end=tk.Label(text="End of exposure (YYYY-mm-dd hh:MM:ss)")
t_end=tk.Entry(root, font="Arial", textvariable=var_t_end)

# choice_current_or_end_time variable (decision between t_end fixed or current fixed)
choice_current_or_end_time=tk.IntVar()
choice_current_or_end_time.trace_add('write', current_or_end_time_fixed_function)
choice_current_or_end_time.set(1)

tk.Label(text=Separator).pack(side=tk.TOP)

# RadioButton for t_end fixed or current fixed
choice_t_end=tk.Radiobutton(root, text="Fixed exposure end time", variable=choice_current_or_end_time, value=1)
choice_t_end.pack(side=tk.TOP)
choice_current=tk.Radiobutton(root, text="Fixed current of X-ray tube", variable=choice_current_or_end_time, value=2)
choice_current.pack(side=tk.TOP)

tk.Label(text=Separator).pack(side=tk.TOP)

# pack t_end and its label
label_t_end.pack(side=tk.TOP)
t_end.pack(side=tk.TOP)

# pack current and its label
label_current.pack(side=tk.TOP)
current.pack(side=tk.TOP)

# Dose rate variables
dose_rate=tk.DoubleVar()
dose_rate_label=tk.Label(textvariable=dose_rate)

tk.Label(text=Separator).pack(side=tk.TOP)

# "Calculate" button
button_calculate=tk.Button(root, text="Calculate", command=calculate)
button_calculate.pack(pady=20)

# Button for closing
exit_button = tk.Button(root, text="Exit", command=root.quit)
exit_button.pack(side=tk.BOTTOM)

root.mainloop()
exit(0)
