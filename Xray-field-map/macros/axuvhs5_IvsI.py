#!/usr/bin/env python

# script to run on the csv file with calibration data from OptoDiode axuvhs5
# data taken on 2023.06.08 using Keithley 2100, HV=-50V
# X-ray tube: primary beam, HV=40 kV


# Conversion I_diode - TID rate in SiO2
# example: TID rate [rad/min] = 50 [rad*nA/min] * I_diode [nA]
# NB: 100 rad/min = 6 Gy/h
# e.g. I_diode = 1000 nA => 30 kGy/h ; 10 Mrad => 3 h

# usage: python axuvhs5_IvsI.py -f <file1.csv,file2.csv,file3.csv>

import os
import array
import ROOT
import csv
from optparse import OptionParser

RAD_TO_KRAD = 0.001

# globals
MARKER_LIST = [(ROOT.kRed,ROOT.kSolid), (ROOT.kBlue,ROOT.kSolid), (ROOT.kGreen+2,ROOT.kSolid), (ROOT.kOrange+1,ROOT.kSolid)]

def readCSV(csvfilename, line_color, line_style, TYPE_TID, VERBOSE):
        if VERBOSE:
            print('=======================')
            print(csvfilename)

        itube = []
        err_itube = []
        idiode = []
        err_idiode = []
        TID = []
        err_TID = []
        with open(csvfilename, 'r') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                itube.append( float(row['I_tube [mA]']) )
                err_itube.append( 0.0)
                idiode.append( float(row['I_diode [nA]']) )
                err_idiode.append( 0.5 )
                K = float(row['Conversion Factor [rad/(min*nA)]'])
                TID.append( K*(float(row['I_diode [nA]'])-float(row['I_dark [nA]']))*RAD_TO_KRAD )
                err_TID.append( K*0.5*RAD_TO_KRAD )

            if TYPE_TID == False:
                gr = ROOT.TGraphErrors(len(itube), array.array('d', itube), array.array('d', idiode), array.array('d', err_itube), array.array('d', err_idiode))
            else:
                gr = ROOT.TGraphErrors(len(itube), array.array('d', itube), array.array('d', TID), array.array('d', err_itube), array.array('d', err_TID))

            gr.SetTitle('Z_{tube} = '+row['Z_tube [mm]']+' mm')
            gr.SetMarkerColor(line_color)
            gr.SetLineColor(line_color)
            gr.SetLineStyle(line_style)
            gr.Fit('pol1','Q')
            gr.GetFunction('pol1').SetNpx(10000)
            gr.GetFunction('pol1').SetLineColor(line_color)
            gr.GetFunction('pol1').SetLineStyle(line_style)
            print('Chi2 {0:5.2f} NDF {1} ProbChi2 {2:5.3f}'.format(gr.GetFunction('pol1').GetChisquare(), gr.GetFunction('pol1').GetNDF(), gr.GetFunction('pol1').GetProb()))
            if VERBOSE:
                for ii in range(len(itube)):
                    print( itube[ii], idiode[ii], gr.GetFunction('pol1')(itube[ii])-idiode[ii] )
            return gr

def main():
        parser = OptionParser(usage="usage: %prog [options] filename",
                             version="%prog 1.0")
        parser.add_option("-f", "--filecsv",
                        type="string",
                        action="store",
                        dest="CSVFILENAMELIST",
                        help="comma separated list of CSV input files")
        parser.add_option("--TID",
                        action="store_true",
                        dest="TYPE_TID",
                        help="current (default) or TID",
                        default=False)
        parser.add_option("-v", "--verbose",
                        action="store_true",
                        dest="VERBOSE",
                        help="verbose",
                        default=False)

        (options, args) = parser.parse_args()

        # declare the ROOT TMultiGraph for I vs. I
        canvas_itube = ROOT.TCanvas('c1_idiode_vs_itube','c1_idiode_vs_itube',600,600)
        mgr_IvsI = ROOT.TMultiGraph()

        # loop on the list of CSV files
        imarker=0
        for csvfilename in options.CSVFILENAMELIST.split(','):
            marker = MARKER_LIST[imarker] # TODO: check if imarker<len(MARKER_LIST)
            mgr_IvsI.Add(readCSV(csvfilename, marker[0], marker[1], options.TYPE_TID, options.VERBOSE), '*')
            imarker+=1

        # draw the TMultiGraph
        mgr_IvsI.Draw('A')
        if options.TYPE_TID == False:
            mgr_IvsI.SetMinimum(0.)
            mgr_IvsI.SetMaximum(1500.)
            mgr_IvsI.GetYaxis().SetTitle('I diode [nA]')
        else:
            mgr_IvsI.SetMinimum(0.)
            mgr_IvsI.SetMaximum(1.)
            mgr_IvsI.GetYaxis().SetTitle('TID SiO_{2} [krad/min]')

        mgr_IvsI.GetXaxis().SetTitle('I tube (nominal) [mA]')
        mgr_IvsI.GetXaxis().SetTitleSize(0.025)
        mgr_IvsI.GetYaxis().SetTitleSize(0.025)
        mgr_IvsI.GetYaxis().SetTitleOffset(2.)
        mgr_IvsI.GetXaxis().SetLabelSize(0.025)
        mgr_IvsI.GetYaxis().SetLabelSize(0.025)

        # create the TLegend (unfortunately it is unsorted...)
        legend_IvsI = ROOT.TLegend(0.15,0.75,0.75,0.85)
        if options.TYPE_TID == False:
            fit_func_txt = '; FIT: I_d [nA]  = {0:4.2f} + {1:4.2f} * i'
        else:
            fit_func_txt = '; FIT: TID SiO2 [krad/min] = {0:5.3f} + {1:5.3f} * i'
        for gr in mgr_IvsI.GetListOfGraphs():
                p0 = gr.GetFunction('pol1').GetParameter(0)
                p1 = gr.GetFunction('pol1').GetParameter(1)
                legend_IvsI.AddEntry(gr,gr.GetTitle()+fit_func_txt.format(p0, p1),'L')
        legend_IvsI.SetTextSize(0.014)
        legend_IvsI.Draw()

        # # Add a right-side axis with values in TID(SiO2)
        # # https://root-forum.cern.ch/t/pyroot-recipe-for-different-left-and-right-axes-for-two-tgraphs/31425
        # # Define the function for right-side axis TID rate [rad/min] = 55.4166 [rad*nA/min] * I_diode [nA]
        # func_TID = ROOT.TF1("fun_TID","0.0554166*x",0., 2000.)
        #
        # # Get the coordinates of the left-side Y axis.
        # uxmax_c1, uymin_c1, uymax_c1 = ROOT.gPad.GetUxmax(), ROOT.gPad.GetUymin(), ROOT.gPad.GetUymax()
        # ymin_c1, ymax_c1 = mgr_IvsI.GetYaxis().GetXmin(), mgr_IvsI.GetYaxis().GetXmax()
        #
        # right_axis_c1 = ROOT.TGaxis(uxmax_c1, uymin_c1, uxmax_c1, uymax_c1, func_TID(ymin_c1), func_TID(ymax_c1), 510, "+L" )
        # right_axis_c1.SetTitle("TID rate in SiO_{2} [krad/min]")
        # right_axis_c1.SetTitleFont(42)
        # right_axis_c1.SetTitleSize(0.025)
        # right_axis_c1.SetTitleOffset(1.5)
        # right_axis_c1.SetLabelFont(42)
        # right_axis_c1.SetLabelSize(0.025)
        # right_axis_c1.Draw()

#--     Add DATE
        t = ROOT.TLatex()
        t.SetTextFont(32)
        t.SetTextSize(0.03)
        t.DrawLatex(mgr_IvsI.GetXaxis().GetXmin() , 1250., '2023.06.08')
        canvas_itube.SetGridx()
        canvas_itube.SetGridy()
        canvas_itube.Update()
        canvas_itube.SaveAs(os.path.join('/tmp',canvas_itube.GetName()+'.pdf'))

##################################
if __name__ == "__main__":
    main()
