# script to add the Occupancy 2D histo produced by Ph2_ACF run in "physics" mode
# usage: python ComputeOccupancyPhysicsRun.py -f [list of input root files, including wildcards]

from optparse import OptionParser
import ROOT
import math
import numpy as np
import array

ChipID = '15'

NREBIN_X = 10
NREBIN_Y = 10

PITCH_X_MM = 0.05
PITCH_Y_MM = 0.05

X_FV_MM = 8
Y_FV_MM = 6

def main():
    parser = OptionParser(usage="usage: %prog [options] filename",
                         version="%prog 1.0")
    parser.add_option("-f", "--fileroot",
                    type="string",
                    action="store",
                    dest="ROOTFILENAME",
                    help="ROOT input file")
    parser.add_option("-v", "--verbose",
                    action="store_true",
                    dest="VERBOSE",
                    help="verbose",
                    default=False)

    (options, args) = parser.parse_args()

    ROOT.gROOT.SetBatch(True)

    if options.VERBOSE:
        print(f'>>> {options.ROOTFILENAME}')
    root_file = ROOT.TFile(options.ROOTFILENAME,'READ')

    # 2D OCC
    # open output ROOT file (TH2 map only)
    root_output_filename = options.ROOTFILENAME.split('.')[0]+'_REBIN.'+options.ROOTFILENAME.split('.')[1]
    root_output_file = ROOT.TFile(root_output_filename,'RECREATE')
    c = root_file.Get('/Detector/Board_0/OpticalGroup_0/Hybrid_0/Chip_'+ChipID+'/D_B(0)_O(0)_H(0)_Occ2D_Chip('+ChipID+')')
	# iterate on all the objetcs in the ROOT file and pick up only the relevant TH2
    for p in c.GetListOfPrimitives():
        if p.InheritsFrom('TH2') and p.GetName() == 'D_B(0)_O(0)_H(0)_Occ2D_Chip('+ChipID+')':
            h2_tmp = c.GetPrimitive('D_B(0)_O(0)_H(0)_Occ2D_Chip('+ChipID+')')
			# rebin 4x4 channels
            h2_tmp_rebin = h2_tmp.Rebin2D(NREBIN_X, NREBIN_Y, h2_tmp.GetName()+'_rebinned')
			# max counts computed in the central 8x8 channels
            ix = int(h2_tmp_rebin.GetXaxis().GetNbins()/2)
            iy = int(h2_tmp_rebin.GetYaxis().GetNbins()/2)
            max_count = h2_tmp_rebin.GetBinContent(ix,iy) + h2_tmp_rebin.GetBinContent(ix,iy+1) + h2_tmp_rebin.GetBinContent(ix+1,iy) + h2_tmp_rebin.GetBinContent(ix+1,iy+1)
			# tot counts computed for all channels
            tot_count = h2_tmp.GetEntries()
            print(max_count, tot_count)

    # TH2 with axis in mm
    nbin_x = h2_tmp.GetXaxis().GetNbins()
    x_min_mm = -PITCH_X_MM*nbin_x/2
    x_max_mm = +PITCH_X_MM*nbin_x/2

    nbin_y = h2_tmp.GetYaxis().GetNbins()
    y_min_mm = -PITCH_Y_MM*nbin_y/2
    y_max_mm = +PITCH_Y_MM*nbin_y/2

    h2_count_mm = ROOT.TH2F('h2_count_mm','h2_count_mm; x [mm]; y [mm]', nbin_x, x_min_mm, x_max_mm, nbin_y, y_min_mm, y_max_mm)

    # fill TH2 from the original fine binned histo
    for ix in range(h2_tmp.GetXaxis().GetNbins()):
        for iy in range(h2_tmp.GetYaxis().GetNbins()):

            weight = h2_tmp.GetBinContent(ix+1, iy+1)

            # this is needed to flip the Y axis
            iy_flip = nbin_y - (iy+1)
            xx = h2_count_mm.GetXaxis().GetBinCenter(ix + 1)
            yy = h2_count_mm.GetYaxis().GetBinCenter(iy_flip + 1)

            h2_count_mm.Fill(xx, yy, weight)

    # get the center of the distribution to remove offset before rotating the TH2
    xx_mean = h2_count_mm.GetMean(1)
    yy_mean = h2_count_mm.GetMean(2)

    # rebin the TH2 to mitigate fluctuations
    h2_count_mm_rebin = h2_count_mm.Rebin2D(NREBIN_X, NREBIN_Y, h2_count_mm.GetName()+'_rebinned')
    print(h2_count_mm_rebin.GetCorrelationFactor(1,2))
    #

    h2_count_mm_rot = h2_count_mm.Clone('h2_count_mm_rot')

    phi_array = array.array('d')
    rho_array = array.array('d')

    rot_min = (0., 1.)
#--
    for phi_deg in np.linspace(-10.,+10, 21):
        phi = math.radians(phi_deg)
        cc = math.cos(phi)
        ss = math.sin(phi)

        # this is the rotated TH2
        h2_count_mm_rot.Reset()
    #
        for ix in range(h2_tmp.GetXaxis().GetNbins()):
            for iy in range(h2_tmp.GetYaxis().GetNbins()):

                weight = h2_tmp.GetBinContent(ix+1, iy+1)

                # this is needed to flip the Y axis
                iy_flip = nbin_y - (iy+1)
                xx = h2_count_mm.GetXaxis().GetBinCenter(ix + 1) - xx_mean
                yy = h2_count_mm.GetYaxis().GetBinCenter(iy_flip + 1) - yy_mean

                xx_rot = xx*cc + yy*ss
                yy_rot = -xx*ss + yy*cc
                # compute correlation in a fiducial (x,y) region to avoid bias
                if math.fabs(xx) <= X_FV_MM and math.fabs(yy) <= Y_FV_MM:
                    h2_count_mm_rot.Fill(xx_rot, yy_rot, weight)

        # rebin the TH2 to mitigate fluctuations
        h2_count_mm_rot_rebin = h2_count_mm_rot.Rebin2D(NREBIN_X, NREBIN_Y, h2_count_mm_rot.GetName()+'_rebinned')
        rho = h2_count_mm_rot_rebin.GetCorrelationFactor(1,2)
        if math.fabs(rho) < math.fabs(rot_min[1]):
            rot_min = (phi_deg, rho)

        phi_array.append(phi_deg)
        rho_array.append(rho)

        if options.VERBOSE:
            print(phi_deg, rho)
#---
    print(rot_min)
    phi = math.radians(rot_min[0])
    cc = math.cos(phi)
    ss = math.sin(phi)

    # this is the rotated TH2
    h2_count_mm_rot.Reset()
    #
    for ix in range(h2_tmp.GetXaxis().GetNbins()):
        for iy in range(h2_tmp.GetYaxis().GetNbins()):

            weight = h2_tmp.GetBinContent(ix+1, iy+1)

            # this is needed to flip the Y axis
            iy_flip = nbin_y - (iy+1)
            xx = h2_count_mm.GetXaxis().GetBinCenter(ix + 1) - xx_mean
            yy = h2_count_mm.GetYaxis().GetBinCenter(iy_flip + 1) - yy_mean

            xx_rot = xx*cc + yy*ss
            yy_rot = -xx*ss + yy*cc
            h2_count_mm_rot.Fill(xx_rot, yy_rot, weight)

    # rebin the TH2 to mitigate fluctuations
    h2_count_mm_rot_rebin = h2_count_mm_rot.Rebin2D(NREBIN_X, NREBIN_Y, h2_count_mm_rot.GetName()+'_rebinned')

    # graph rho vs. phi
    gr = ROOT.TGraph(len(phi_array), phi_array, rho_array)
    gr.SetMarkerStyle(ROOT.kFullTriangleUp)
    # save the TH2 to output ROOT file
    gr.Write()
    h2_tmp.Write()
    h2_tmp_rebin.Write()
    h2_count_mm.Write()
    # rescale TH2 before writing to root output file to have relative occupancies
    h2_count_mm_rebin.SetMinimum(0.)
    h2_count_mm_rebin.Scale( 1./h2_count_mm_rebin.GetMaximum() )
    count_levels = np.linspace(0., 1., 11)
    h2_count_mm_rebin.SetContour(11, count_levels)
    h2_count_mm_rebin.Write()
    h2_count_mm_rot.Write()
    # rescale TH2 before writing to root output file to have relative occupancies
    h2_count_mm_rot_rebin.SetMinimum(0.)
    h2_count_mm_rot_rebin.Scale( 1./h2_count_mm_rot_rebin.GetMaximum() )
    h2_count_mm_rot_rebin.SetContour(11, count_levels)
    h2_count_mm_rot_rebin.Write()

    root_output_file.Close()

##################################
if __name__ == "__main__":
    main()
