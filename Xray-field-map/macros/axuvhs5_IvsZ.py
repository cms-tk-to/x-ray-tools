#!/usr/bin/env python

# script to run on the csv file with calibration data from OptoDiode axuvhs5
# data taken on 2023.06.08 using Keithley 2100, HV=-50V
# X-ray tube: primary beam, HV=40 kV


# Conversion I_diode - TID rate in SiO2
# example: TID rate [rad/min] = 50 [rad*nA/min] * I_diode [nA]
# NB: 100 rad/min = 6 Gy/h
# e.g. I_diode = 1000 nA => 30 kGy/h ; 10 Mrad => 3 h

# usage: python axuvhs5_IvsZ.py -f <file1.csv,file2.csv,file3.csv>

import os
import array
import ROOT
import csv
from optparse import OptionParser

# globals
MARKER_LIST = [(ROOT.kRed,ROOT.kSolid), (ROOT.kBlue,ROOT.kSolid), (ROOT.kGreen+2,ROOT.kSolid),
               (ROOT.kOrange+1,ROOT.kSolid), (ROOT.kBlue-1,ROOT.kSolid), (ROOT.kGreen,ROOT.kSolid)]

RAD_TO_KRAD = 0.001


def readCSV(csvfilename, line_color, line_style, TYPE_TID, VERBOSE):
        if VERBOSE:
            print('=======================')
            print(csvfilename)

        height = []
        err_height = []
        idiode = []
        err_idiode = []
        TID = []
        err_TID = []
        xextra = []
        yextra = []

        with open(csvfilename, 'r') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                if float(row['Z_tube [mm]'])>26.:
                    height.append( float(row['Z_tube [mm]'])+float(row['Z_offset [mm]']) )
                    err_height.append( 0.0 )
                    idiode.append( float(row['I_diode [nA]'])-float(row['I_dark [nA]']) )
                    err_idiode.append( 0.5 )

                    K = float(row['Conversion Factor [rad/(min*nA)]'])
                    TID.append( K*(float(row['I_diode [nA]'])-float(row['I_dark [nA]']))*RAD_TO_KRAD )
                    err_TID.append( K*0.5*RAD_TO_KRAD )
                else :
                    xextra.append(float(row['Z_tube [mm]'])+float(row['Z_offset [mm]']))
                    yextra.append(float(row['I_diode [nA]'])-float(row['I_dark [nA]']) )
                char_diode = row['Diode']

            if TYPE_TID == False:
                gr = ROOT.TGraphErrors(len(height), array.array('d', height), array.array('d', idiode), array.array('d', err_height), array.array('d', err_idiode))
            else:
                gr = ROOT.TGraphErrors(len(height), array.array('d', height), array.array('d', TID), array.array('d', err_height), array.array('d', err_TID))

            fit_func = ROOT.TF1('fit_func', '[0] + [1]/((x+[2])**2)', 0., 500.)
            if TYPE_TID == False:
                fit_func.SetParameters(0., 1e6, 50.)
            else:
                fit_func.SetParameters(0., 1e5, 50.)

            fit_func.SetNpx(50000)
            fit_func.SetLineColor(line_color)
            fit_func.SetLineStyle(line_style)
            gr.Fit('fit_func','R')
            print('Chi2 {0:4.1f} NDF {1} ProbChi2 {2:4.2f}'.format(fit_func.GetChisquare(), fit_func.GetNDF(), fit_func.GetProb()))
            if VERBOSE:
                for ii in range(len(height)):
                    print( height[ii], idiode[ii], fit_func.Eval(height[ii])-idiode[ii] )

            #add points skipped in the fit
            for ix in range(len(xextra)):
                gr.AddPoint(xextra[ix],yextra[ix])
                gr.SetPointError(gr.GetN()-1,0.0,0.5)
            gr.Sort()
            gr.SetTitle(char_diode+' I_{tube} = '+row['I_tube [mA]']+' mA')
            gr.SetMarkerColor(line_color)
            gr.SetLineColor(line_color)
            gr.SetLineStyle(line_style)

            return gr

def main():
        parser = OptionParser(usage="usage: %prog [options] filename",
                             version="%prog 1.0")
        parser.add_option("-f", "--filecsv",
                        type="string",
                        action="store",
                        dest="CSVFILENAMELIST",
                        help="comma separated list of CSV input files")
        parser.add_option("--TID",
                        action="store_true",
                        dest="TYPE_TID",
                        help="current (default) or TID",
                        default=False)
        parser.add_option("-v", "--verbose",
                        action="store_true",
                        dest="VERBOSE",
                        help="verbose",
                        default=False)

        (options, args) = parser.parse_args()

        # declare the ROOT TMultiGraph for I vs. z
        canvas_ztube = ROOT.TCanvas('c1_idiode_vs_ztube','c1_idiode_vs_ztube',600,600)
        mgr_IvsZ = ROOT.TMultiGraph()

        # loop on the list of CSV files
        imarker=0
        for csvfilename in options.CSVFILENAMELIST.split(','):
            marker = MARKER_LIST[imarker] # TODO: check if imarker<len(MARKER_LIST)
            mgr_IvsZ.Add(readCSV(csvfilename, marker[0], marker[1], options.TYPE_TID, options.VERBOSE), '*')
            imarker+=1

        # draw the TMultiGraph
        mgr_IvsZ.Draw('A')
        mgr_IvsZ.GetXaxis().SetTitle('#DeltaZ [mm]')
        mgr_IvsZ.GetXaxis().SetLimits(0.,500.)
        if options.TYPE_TID == False:
            mgr_IvsZ.SetMinimum(0.)
            mgr_IvsZ.SetMaximum(1500.)
            mgr_IvsZ.GetYaxis().SetTitle('I diode [nA]')
        else:
            mgr_IvsZ.SetMinimum(0.)
            mgr_IvsZ.SetMaximum(100.)
            mgr_IvsZ.GetYaxis().SetTitle('TID SiO_{2} [krad/min]')

        mgr_IvsZ.GetXaxis().SetTitleSize(0.025)
        mgr_IvsZ.GetYaxis().SetTitleSize(0.025)
        mgr_IvsZ.GetYaxis().SetTitleOffset(2.)
        mgr_IvsZ.GetXaxis().SetLabelSize(0.025)
        mgr_IvsZ.GetYaxis().SetLabelSize(0.025)


        # create the TLegend (unfortunately it is unsorted...)
        if options.TYPE_TID == False:
           fit_func_txt = '; FIT: I_d [nA] = {0:4.1f} + {1:3.1e}/(#DeltaZ+{2:5.1f})^2'
        else:
           fit_func_txt = '; FIT: TID SiO2 [krad/min] = {0:4.1f} + {1:3.1e}/(#DeltaZ+{2:5.1f})^2'

        legend_IvsZ = ROOT.TLegend(0.35,0.65,0.85,0.80)
        for gr in mgr_IvsZ.GetListOfGraphs():
            p0 = gr.GetFunction('fit_func').GetParameter(0)
            p1 = gr.GetFunction('fit_func').GetParameter(1)
            p2 = gr.GetFunction('fit_func').GetParameter(2)
            legend_IvsZ.AddEntry(gr,gr.GetTitle()+fit_func_txt.format(p0, p1, p2),'L')
        legend_IvsZ.SetTextSize(0.017)
        legend_IvsZ.Draw()


        # # Add a right-side axis with values in TID(SiO2)
        # # https://root-forum.cern.ch/t/pyroot-recipe-for-different-left-and-right-axes-for-two-tgraphs/31425
        # # Define the function for right-side axis TID rate [rad/min] = 55.4166 [rad*nA/min] * (I_diode - 2.2)*[nA]
        # func_TID = ROOT.TF1("fun_TID","0.0554166*(x-2.78)",0., 2000.)
        #
        # # Get the coordinates of the left-side Y axis.
        # uxmax_c1, uymin_c1, uymax_c1 = ROOT.gPad.GetUxmax(), ROOT.gPad.GetUymin(), ROOT.gPad.GetUymax()
        # ymin_c1, ymax_c1 = mgr_IvsZ.GetYaxis().GetXmin(), mgr_IvsZ.GetYaxis().GetXmax()
        #
        # right_axis_c1 = ROOT.TGaxis(uxmax_c1, uymin_c1, uxmax_c1, uymax_c1, func_TID(ymin_c1), func_TID(ymax_c1), 510, "+L" )
        # right_axis_c1.SetTitle("TID rate in SiO_{2} [krad/min]")
        # right_axis_c1.SetTitleFont(42)
        # right_axis_c1.SetTitleSize(0.025)
        # right_axis_c1.SetTitleOffset(1.5)
        # right_axis_c1.SetLabelFont(42)
        # right_axis_c1.SetLabelSize(0.025)
        # right_axis_c1.Draw()

#--     Add DATE
        t = ROOT.TLatex()
        t.SetTextFont(32)
        t.SetTextSize(0.03)
        t.DrawLatex(mgr_IvsZ.GetXaxis().GetXmin() , 1250., '2023.06.08')
        canvas_ztube.SetGridx()
        canvas_ztube.SetGridy()
        canvas_ztube.Update()
        canvas_ztube.SaveAs(os.path.join('/tmp',canvas_ztube.GetName()+'.root'))
        canvas_ztube.SaveAs(os.path.join('/tmp',canvas_ztube.GetName()+'.pdf'))

##################################
if __name__ == "__main__":
    main()
