import math
import os

# python modules for PI controller
import pipython
from pipython import GCSDevice, pitools
from pipython.pidevice.gcsmessages import GCSMessages
from pipython.pidevice.gcscommands import GCSCommands
from pipython.pidevice.interfaces.piserial import PISerial
import numpy as np

# Keithley module from ICICLE
import icicle.keithley2410

# ROOT
import ROOT

# command line options
from optparse import OptionParser

# constants
A_TO_nA = 1e9

# variables for change of reference frame
LASER_NATIVE = (105-1.30+0.2,70-0.60+0.3) # [mm]
LASER_CROC = (0,0)                # [mm]
THETA = math.pi    
Y_PARK_BACK  = +65 # "positive" parking position towards the back of the cabinet
Y_PARK_FRONT = -80 # "negative" parking position towards the door of the cabinet

def native_to_croc(x_native, y_native):
# translation to center at laser crosshair
    x_translated = x_native - LASER_NATIVE[0]
    y_translated = y_native - LASER_NATIVE[1]
    
    # rotation if needed (set THETA to if no rotation)
    x_croc = x_translated * math.cos(THETA) - y_translated * math.sin(THETA)
    y_croc = x_translated * math.sin(THETA) + y_translated * math.cos(THETA)

    # translate to CROC origin 
    x_croc += LASER_CROC[0]
    y_croc += LASER_CROC[1]
    return  x_croc, y_croc 
    
def croc_to_native(x_croc, y_croc):
    x_translated = x_croc - LASER_CROC[0]
    y_translated = y_croc - LASER_CROC[1]
    
    x_native = x_translated * math.cos(-THETA) - y_translated * math.sin(-THETA)
    y_native = x_translated * math.sin(-THETA) + y_translated * math.cos(-THETA)
    
    # translate to native origin 
    x_native += LASER_NATIVE[0]
    y_native += LASER_NATIVE[1]
    return x_native, y_native
    
    
def read_grid_from_ascii_file(file_name):
    grid_points = []
    
    with open(file_name, "r") as file:
       for line in file:
           coordinates = line.split() #split the line into two parts seperated by a space
           x = float(coordinates[0].strip()) # use strip() used to remove extra spaces before converting the values to float
           y = float(coordinates[1].strip()) # same for y
           grid_points.append((x,y)) #append the x,y tuples to the grid_points list    
    return grid_points

     
def move_to_xypos(pidevice_x, pos_x_setting, pidevice_y, pos_y_setting, change_to_CROC_coordinates=False, verbose=False):
    pos_x_native, pos_y_native = pos_x_setting, pos_y_setting
    if change_to_CROC_coordinates:
       # convert the CROC coordinates to native coordinates
       pos_x_native, pos_y_native = croc_to_native(pos_x_setting, pos_y_setting)
       if verbose:
          print(f"Translated to native coordinates: ({pos_x_native}, {pos_y_native})")

# move to native position in x and y axes
    pitools.moveandwait(pidevice_x, 1, pos_x_native)  # "1" as mercury controllers are not daisy chained (tbc)
    pitools.moveandwait(pidevice_y, 1, pos_y_native)
    return

def main():

# command line options
   parser = OptionParser()

   parser.add_option("-v", action="store_true", dest="verbose",
                    help="verbose mode - it opens a canvas to monitor the progress during the scan") 

   parser.add_option("-D", "--diode", dest="diode", action="store", type="string",
                     help="diode for I to TID conversion - possible choices: D1 (default), D3, D5", default='D1')

   parser.add_option("-G", "--grid_input_filename", dest="grid_input_filename", action="store", type="string",
                  help="name of grid input file", default="grid_default.txt")
                  
   parser.add_option("-R", "--root_output_filename", dest="root_output_filename", action="store", type="string",
                  help="name of ROOT output file", default="test-XY.root")

   parser.add_option("-C", "--CROC-frame", action="store_true", dest="ChangeToCROCcoordinates",
                  help="creates a grid in the reference frame of the CROC (default: x,y coordinates are assumed to be in the PI native reference frame")
                  
   (options, args) = parser.parse_args()

# call the function to read the grid_points from ascii file
   grid_points = read_grid_from_ascii_file(os.path.join('GRIDS',options.grid_input_filename))

# print the grid_points
   if options.verbose:
      for x,y in grid_points:
         print(x,y)

# https://stackoverflow.com/questions/12974474/how-to-unzip-a-list-of-tuples-into-individual-lists
# https://stackoverflow.com/questions/12897374/get-unique-values-from-a-list-in-python
   xbins_tmp = sorted(list(set([list(t) for t in zip(*grid_points)][0])))
   dx = (xbins_tmp[-1]-xbins_tmp[0])/(len(xbins_tmp)-1)
   ybins_tmp = sorted(list(set([list(t) for t in zip(*grid_points)][1])))
   dy = (ybins_tmp[-1]-ybins_tmp[0])/(len(ybins_tmp)-1)

# init ROOT stuff
   root_output_file = ROOT.TFile(os.path.join('/tmp',options.root_output_filename),'RECREATE')
   n_diode = ROOT.TNamed('diode',options.diode)
   h2_iref_back_xymap  = ROOT.TH2F('h2_iref_back_xymap',  'h2_iref_back_xymap [nA]; x [mm]; y [mm]',  len(xbins_tmp), xbins_tmp[0]-0.5*dx, xbins_tmp[-1]+0.5*dx, len(ybins_tmp), ybins_tmp[0]-0.5*dy, ybins_tmp[-1]+0.5*dy)
   h2_iref_front_xymap = ROOT.TH2F('h2_iref_front_xymap', 'h2_iref_front_xymap [nA]; x [mm]; y [mm]', len(xbins_tmp), xbins_tmp[0]-0.5*dx, xbins_tmp[-1]+0.5*dx, len(ybins_tmp), ybins_tmp[0]-0.5*dy, ybins_tmp[-1]+0.5*dy)
   h2_iraw_xymap = ROOT.TH2F('h2_iraw_xymap', 'h2_iraw_xymap [nA]; x [mm]; y [mm]', len(xbins_tmp), xbins_tmp[0]-0.5*dx, xbins_tmp[-1]+0.5*dx, len(ybins_tmp), ybins_tmp[0]-0.5*dy, ybins_tmp[-1]+0.5*dy)
   if options.verbose:
      canvas = ROOT.TCanvas()

# open PI stages
# open Keithley PS as an ICICLE objects
   with PISerial(port='/dev/mercury-023550031', baudrate=115200) as gateway_x,\
        PISerial(port='/dev/mercury-023550149', baudrate=115200) as gateway_y,\
        icicle.keithley2410.Keithley2410(resource = 'ASRL/dev/smu24xx::INSTR', reset_on_init=False) as smu:
         smu.selftest()
         if options.verbose:
            print(smu.identify())
       
         messages_x, messages_y = GCSMessages(gateway_x), GCSMessages(gateway_y)
         
         with GCSCommands(messages_x) as pidevice_x,GCSCommands(messages_y) as pidevice_y:

# 'FPL' = 'find positive limit'  (to be confirmed...)
# please expand here what does it mean
           pitools.startup(pidevice_x, None , 'FPL', True)
           pitools.startup(pidevice_y, None , 'FPL', True)
           pitools.waitonreferencing(pidevice_x)
           pitools.waitonreferencing(pidevice_y)

# loop on the grid           
           for x, y in grid_points:
           # for x, y in reversed(grid_points):

             # store reference value for the current in a reference position / FRONT
             if y == ybins_tmp[0]:
                 move_to_xypos(pidevice_x, x, pidevice_y, Y_PARK_FRONT, options.ChangeToCROCcoordinates, options.verbose)
                 #i_nA = smu.measure_single()[1]*A_TO_nA
                 i_nA = smu.measure(repetitions=4)[1]*A_TO_nA
                 for ytmp in ybins_tmp:
                     h2_iref_front_xymap.Fill(x, ytmp, math.fabs(i_nA))
  
             move_to_xypos(pidevice_x, x, pidevice_y, y, options.ChangeToCROCcoordinates, options.verbose)
             #i_nA = smu.measure_single()[1]*A_TO_nA
             i_nA = smu.measure(repetitions=4)[1]*A_TO_nA
             h2_iraw_xymap.Fill(x, y, math.fabs(i_nA))
             if options.verbose:
                 h2_iraw_xymap.Draw()
                 canvas.Update()
                
             # store reference value for the current in a reference position / BACK
             if y == ybins_tmp[-1]:
                 move_to_xypos(pidevice_x, x, pidevice_y, Y_PARK_BACK, options.ChangeToCROCcoordinates, options.verbose)
                 #i_nA = smu.measure_single()[1]*A_TO_nA
                 i_nA = smu.measure(repetitions=4)[1]*A_TO_nA
                 for ytmp in ybins_tmp:
                     h2_iref_back_xymap.Fill(x, ytmp, math.fabs(i_nA))
                
   if options.verbose:
   	input('Enter any key to terminate the script')
         
# save ROOT output file
   n_diode.Write()
   h2_iraw_xymap.Write()
   h2_iref_back_xymap.Write()
   h2_iref_front_xymap.Write()
   root_output_file.Close()

##################################
if __name__ == "__main__":
    main()
