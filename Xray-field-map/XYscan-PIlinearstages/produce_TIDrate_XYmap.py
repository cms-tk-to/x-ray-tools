slope_dict   = {'D1':55.4166, 'D3':56.3033, 'D5':55.0451} # [rad/(min*nA)]
offset_dict  = {'D1':2.2,     'D3':5.0,     'D5':17.0}    # [nA]

from argparse import ArgumentParser

import ROOT

parser = ArgumentParser()
parser.add_argument(
        "input_file_name",
        help="Input ROOT file where calibrated diode current measurements are stored")
parser.add_argument(
        "output_file_name",
        help="Output ROOT file where TID rate measurements shall be stored")
parser.add_argument(
        "--tube-current",
        help="X-ray tube current in mA",
        type=float, default=1.)
args = parser.parse_args()

with ROOT.TFile(args.input_file_name) as input_file, ROOT.TFile(args.output_file_name, 'RECREATE') as output_file:
    # fetch objects from input ROOT file
    the_diode        = input_file.Get("diode").GetTitle()
    h2_offset_front  = input_file["h2_iref_front_xymap"]
    h2_offset_back   = input_file["h2_iref_back_xymap"]
    h2_current       = input_file["h2_iraw_xymap"]

#    flat_offset = ROOT.TF2("flat_offset", lambda *_: offset_dict[the_diode], # args.diode],
#            float("-inf"), float("+inf"), float("-inf"), float("+inf")) # Set |R**2 as function range
#    h2_current.Add(flat_offset, -1.)
    
# correction for the dark current is the average of currents measured on back and front parking postions
    h2_current_calib = h2_current.Clone("h2_icalib_xymap")   
    h2_current_calib.Add(h2_offset_front, -0.5)
    h2_current_calib.Add(h2_offset_back,  -0.5)    
    h2_current_calib.Scale(1. / args.tube_current)

    # Clone input histogram to have same axes and binning
    h2_tid_rad_min = h2_current_calib.Clone("h2_tid_xymap_rad_min")
    h2_tid_megarad_hour = h2_current_calib.Clone("h2_tid_xymap_megarad_hour")

    # Clear all content
    h2_tid_rad_min.Reset()
    h2_tid_megarad_hour.Reset()

    # Adjust titles (they were also cloned from the original histogram)
    h2_current_calib.SetTitle("h2_current calibrated [nA] ")  # I_dark subtracted and normalized for I_tube=1 mA
    h2_tid_rad_min.SetTitle("h2_tid_xymap [rad/(min*mA)]")
    h2_tid_megarad_hour.SetTitle("h2_tid_xymap [Mrad/(h*mA)]")

    h2_tid_rad_min.Add(h2_current_calib)
    h2_tid_rad_min.Scale(slope_dict[the_diode]) # args.diode])
    
    MEGARAD_PER_HOUR_IN_RAD_PER_MIN = 60. / 1e6
    h2_tid_megarad_hour.Add(h2_tid_rad_min, MEGARAD_PER_HOUR_IN_RAD_PER_MIN)

    h2_current_calib.Write()
    h2_tid_rad_min.Write()
    h2_tid_megarad_hour.Write()
    
    # save the inputs in a subfolder
    inputs_dir = output_file.mkdir('inputs')
    inputs_dir.cd()
    n_diode = ROOT.TNamed('diode',the_diode)
    n_diode.Write()
    n_itube_mA = ROOT.TParameter(float)('itube_mA',args.tube_current)
    n_itube_mA.Write()
    h2_offset_front.Write()
    h2_offset_back.Write()
    h2_current.Write()
    output_file.Close()
