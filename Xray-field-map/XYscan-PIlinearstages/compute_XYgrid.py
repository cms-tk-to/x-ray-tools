# python script to compute a (x,y) grid and write it to an ASCII file
# the PI linear stages are assumed to move to the (x,y) points listed in the grid
import numpy as np
import os

# command line options
from optparse import OptionParser

#
def make_grid_and_save_to_file(min_x, max_x, min_y, max_y, n_step_x, n_step_y, output_file):
    x_values = np.linspace(min_x, max_x, n_step_x+1)
    y_values = np.linspace(min_y, max_y, n_step_y+1)
    
    grid_points = []
    for x in x_values:
        for y in y_values:
            grid_points.append((x,y))
            
# write the grid points to an ASCII file   
    with open(output_file, "w") as file:
     for x,y in grid_points:
        file.write(f"{x} {y}\n")

    return grid_points
    

def main():    

# command line options
    parser = OptionParser()
                      
    parser.add_option("-G", "--grid_output_filename", dest="grid_output_filename", action="store", type="string",
                  help="name of grid output file", default="grid_default.txt")
 
    (options, args) = parser.parse_args()

# TODO these have to become command line options
# create actual grid points     
    min_x =  -4 # [mm]
    max_x =   2 # [mm]
    min_y =  -2 # [mm]
    max_y =   6 # [mm]
    n_step_x = 6*2 #
    n_step_y = 8*2 # 

# create the grid and save it to the file (assumed to be in the subfolder GRIDS)
    grid_x_y = make_grid_and_save_to_file(min_x, max_x, min_y, max_y, n_step_x, n_step_y, os.path.join('GRIDS',options.grid_output_filename))

# print the filename for reference
    print(f"grid points saved to {options.grid_output_filename}")

##################################
if __name__ == "__main__":
    main()
