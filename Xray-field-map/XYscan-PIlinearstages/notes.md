we have 3 scripts
- step 1 (compute_grid.py): creating the grids 
- step 2 (make-XYradmap.py): performing the scan and taking the (raw) measurements of Idiode with the Keithley SMU
- step 3 (produce_TIDrate_XYmap.py): computing TID map in Mrad/(h*mA) from raw Idiode measurements

the script that creates the radiation maps has to
- read the grid
- perform stage movement ---> can be done only in the coords system which is native to the linear stages, meaning:
  - 0mm is as close as possible to the motor
  - 150mm is as far away as possible form the motor
- create the radiation map


in principle we want to allow for different coordinate system from the native one, e.g. the one that is used in the maps that were acquired using the CROC
so we need to perform transformations between different coords systems (because we need at least the native system + one other)

*where should this transformation be performed?*

imo this should only happen in make-XYradmap.py, because it *has* to happen there (as it has to do the movement (native), and it has to produce the map ("CROC"))
and it makes no sense to duplicate it.

so my proposal is that the compute_grid.py be coords-system-agnostic
- compute_grid.py should take the boundary and the other grid parameters either from the command line, OR from script itself ---> no mixture


# About reference frames

The native reference frames is as follows:

                                                                                                                                                                                                        
                                                                                                                                                                                                        
                                                            BACK OF THE CABINET                                                                                                                         
                                                                                                                                                                                                        
           |----------------------------------------------------------------------------------------------------------------------------------------------------------|                                 
           |                                                                                                                                                          |                                 
           |                                                                       <--------------------- x                                      |                    |                                 
           |                                                                                                                                     |                    |                                 
           |                            (x=150,y=0)                                                                    (x=0,y=0)                 |                    |                                 
           |                                                                                                                                     |                    |                                 
           |                                  |--------------------------------------------------------------------------|                       |                    |                                 
           |                                  |                                                                          |                       |                    |                                 
           |                                  |                                                                          |                       |                    |                                 
 WORKSHOP  |                                  |                                                                          |                       |                    |    ZEN GARDEN                   
           |                                  |                         + <---- LASER CROSSHAIR (x=105,y=70)             |                       |                    |                                 
           |                                  |                                                                          |                       |                    |                                 
           |                                  |                                                                          |                       |                    |                                 
           |                                  |                                                                          |                       |                    |                                 
           |                                  |                                                                          |                       |                    |                                 
           |                                  |                                                                          |                       | y                  |                                 
           |                                  |--------------------------------------------------------------------------|                       V                    |                                 
           |                                                                                                                (x=0,y=150)                               |                                 
           |                            (x=150,y=150)                                                                                                                 |                                 
           |                                                                                                                                                          |                                 
           |                                                                                                                                                          |                                 
           |                                                                                                                                                          |                                 
           |                                                                                                                                                          |                                 
           |----------------------------------------------------------------------------------------------------------------------------------------------------------|                                 
                                                                                                                                                                                                        
                                                                              DOOR                                                                                                                      
                                                                                                                                                                                                        



CROC reference frame:
                                                                                                                                                                                                        
                                                                                                                                                                                                        
                                                            BACK OF THE CABINET                                                                                                                         
                                                                                                                                                                                                        
           |----------------------------------------------------------------------------------------------------------------------------------------------------------|                                 
           |                                                                                                                                                          |                                 
           |                                                                                                                                                          |                                 
           |                                                                            ------------------------------> x                          ^                  |                                 
           |                               (x=-45,y=+70)                                                                                           | y                |                                 
           |                                                                                                               (x=105,+70)             |                  |                                 
           |                                  |--------------------------------------------------------------------------|                         |                  |                                 
           |                                  |                                                                          |                         |                  |                                 
           |                                  |                                                                          |                         |                  |                                 
 WORKSHOP  |                                  |                                                                          |                         |                  |    ZEN GARDEN                   
           |                                  |                         + <---- LASER CROSSHAIR (x=0,y=0)                |                         |                  |                                 
           |                                  |                                                                          |                         |                  |                                 
           |                                  |                                                                          |                         |                  |                                 
           |                                  |                                                                          |                         |                  |                                 
           |                                  |                                                                          |                         |                  |                                 
           |                                  |                                                                          |                         |                  |                                 
           |                                  |--------------------------------------------------------------------------|                         |                  |                                 
           |                                                                                                               (x=105,y=-80)                              |                                 
           |                               (x=-45,y=-80)                                                                                                              |                                 
           |                                                                                                                                                          |                                 
           |                                                                                                                                                          |                                 
           |                                                                                                                                                          |                                 
           |                                                                                                                                                          |                                 
           |----------------------------------------------------------------------------------------------------------------------------------------------------------|                                 
                                                                                                                                                                                                        
                                                                              DOOR                                                                                                                      
                                                                                                                                                                                                        
