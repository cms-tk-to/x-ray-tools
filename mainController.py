### This macro runs the various steps needed for the calibration

### STILL DRAFT

import sys, pwd, commands, optparse
import os,subprocess
from subprocess import check_output
import re
import math
import string
import xml.etree.ElementTree as ET

from outParser import *

def parseOptions():

    usage = ('usage: %prog [options] datasetList\n'
             + '%prog -h for help')
    parser = optparse.OptionParser(usage)
    
    parser.add_option('-i', '--input', dest='inputFile', type='string', default="threshold.log", help='inputfile')
    parser.add_option('-t', '--threshold', dest='threshold', type='string', default="6000", help='threshold of the source')
    parser.add_option('-s', '--step', dest='calibStep', type=int, default=1, help='1=Vthreshold_LIN')

    # store options and arguments as global variables
    global opt, args
    (opt, args) = parser.parse_args()


parseOptions()
#load xml default file  
tree = ET.parse('CMSIT.xml')
root = tree.getroot()

if opt.calibStep == 1 : 
	#Run the first adjustment with  the correct settings
	#<Setting name="nEvents">            100 </Setting>
	#<Setting name="nEvtsBurst">         100 </Setting>
	#<Setting name="INJtype">              1 </Setting>
	#<Setting name="TargetThr">         2000 </Setting>
	#<Setting name="ResetTDAC">            1 </Setting>
	nev = root.find("./Settings/Setting/[@name='nEvents']")
	neb = root.find("./Settings/Setting/[@name='nEvtsBurst']")
	inj = root.find("./Settings/Setting/[@name='INJtype']")
	tar = root.find("./Settings/Setting/[@name='TargetThr']")
	tda = root.find("./Settings/Setting/[@name='ResetTDAC']")
	nev.text = '   100'
	neb.text = '   100'
	inj.text = '     1'
	tar.text = opt.threshold
	tda.text = '     1'
	outString = "CMSIT_step1_0_"+opt.threshold+".xml"
	tree.write(outString)

	#execute
	#proc = subprocess.Popen("CMSITminiDAQ -f "+outString+" -c thradj",stdout=subprocess.PIPE,shell=True)
	proc = subprocess.Popen("ls -lh",stdout=subprocess.PIPE,shell=True)
	out,err= proc.communicate()
	#out = check_output("ls -lh",shell=True).decode("utf-8")
	#get VLin value from stderr
	Vlin = getValue(out, 1)
	print Vlin
	#Write it in the xml