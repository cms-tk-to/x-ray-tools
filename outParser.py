### This macro scans the stdout generate by the given step of the calibration procedure for the relevant
### informations

### STILL DRAFT

import sys, pwd, commands, optparse
import os
import re
import math
import string
import fileinput

def getValue(inputFile, step):
	textToSearch = ""
	if step == 1 : textToSearch = "Vthreshold_LIN"

	outLine = ""
	for line in inputFile.splitlines() :
		#print line
		if not textToSearch in line : continue
		if "Global threshold for" in line : 
			outLine = line
			break
	words = outLine.split()
	return words[0]

