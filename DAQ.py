import os
import subprocess
import xml.etree.ElementTree as ET

class DAQ:
    """Constructor.

    Args:
        daqexe: Path to the CMSITminiDAQ executable.
        rundir: Path to the directory where configuration files and
          results are saved.
    """
    def __init__(self, daqexe, rundir):
        self.daqexe = os.path.abspath(daqexe)
        self.rundir = os.path.abspath(rundir)
        self.xmlpath = os.path.join(self.rundir, "CMSIT.xml")
        self.xmlpath = os.path.abspath(self.xmlpath)
        self.xmltree = ET.parse(self.xmlpath)
        self.xmlroot = self.xmltree.getroot()


    """Runs the requested calibration.

    Runs the requested calibration from within the rundir provided at
    initialization, with the configuration files stored in the same directory.

    Args:
        calibname: Name of the calibration to run, passed to CMSITminiDAQ via
          the '-c' option.

    Returns:
        A CompletedProcess instance through which one can access the standard
        output for the requested calibration after it's done running.
    """
    def run_calib(self, calibname):
        # Changing the current working directory via subprocess.run() does not
        # update the PWD environment variable, which is used by the DAQ and thus
        # needs to be manually set to the proper value.
        envar = os.environ.copy()
        envar["PWD"] = os.path.abspath(self.rundir)
        # Run the DAQ software.
        cmd = [self.daqexe, "-f", self.xmlpath, "-c", calibname]
        return subprocess.run(cmd, cwd=self.rundir, stdout=subprocess.PIPE, env=envar)


    """Returns the current run number.

    Returns the current run number, to be assigned to the next calibration to
    run. It is returned as a string, with as many leading zeros as it should
    show with as part of a result file's name.

    Returns:
        The current run number as a string.
    """
    def get_run_number(self):
        filepath = os.path.join(self.rundir, "RunNumber.txt")
        try:
            runnumberfile = open(filepath, "r") # Crash in case of error.
            # Use rstrip() to discard trailing newline.
            return runnumberfile.readline().rstrip()
        except:
            return "000000"


    """Returns the value of one parameter used in a calibration.

    Returns the value of one parameter which is used by a calibration

    Args:
        parname: Name of the parameter whose value is returned.

    Returns:
        The configured value for the requested parameter as a string, or None
        if there is no such parameter in the configuration file.
    """
    def get_calib_param(self, parname):
        # Calibration parameter values are read from the first Settings element
        # alone ([1] in XPath notation).
        params = self.xmlroot.findall(f"Settings[1]/Setting[@name='{parname}']")
        if params: # Non-empty list case
            # In case for the same parameter several different values are
            # provided, the value which is valid is the last to appear.
            return params[-1].text
        else: # Empty list case
            return None


    """Updates the configuration for calibrations.

    Updates the sections of the XML configuration file containing the settings
    for all calibrations.

    Args:
        config: A dictionary with all the settings to update. Each key is the
          name of one setting, and its value can be of any type; the XML file
          shall contain its string representation via str().
    """
    def config_calibs(self, config):
        # Calibration parameter values are read from the first Settings element
        # alone.
        params = self.xmlroot.find("Settings") # FIXME Check for None.
        for name, val in config.items():
            # TODO Remove overridden parameter values.
            ET.SubElement(params, "Setting", {"name": name}).text = str(val)
        self.xmltree.write(self.xmlpath)


    """Updates the configuration rules for all chips.

    Updates the sections of the XML configuration file containing the settings
    for all chips.

    Args:
        config: See the argument with the same name in config_calibs().
    """
    def config_chips(self, config):
        # Update the XML file with the requested configuration.
        for chip in self.xmlroot.findall("BeBoard/OpticalGroup/Hybrid/*[@Lane]"):
            params = chip.find("Settings") # FIXME Check for None.
            for name, val in config.items():
                params.set(name, str(val))
        self.xmltree.write(self.xmlpath)


    """Updates the configuration rules for all FC7 boards.

    Updates the section of the XML configuration file containing the settings
    for all FC7 boards.

    Args:
        config: A dictionary with all the settings to update. Each key is the
          name of one register of the FC7, i.e. its full path in the uHAL
          address table (e.g. "user.stat_regs.fw_date.month", not just "month");
          the value to be stored in the register is the string representation
          of the one in the dictionary via str().
    """
    def config_fc7(self, config):
        for board in self.xmlroot.findall("BeBoard"):
            for reg, val in config.items():
                ET.SubElement(board, "Register", {"name": reg}).text = str(val)
        self.xmltree.write(self.xmlpath)
