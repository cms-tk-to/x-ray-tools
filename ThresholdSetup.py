import argparse
import logging
import os
import re
import shutil

import ROOT

import DAQ

def checkTDAC(root_file, chip_id=0):
    dir = f'/Detector/Board_0/OpticalGroup_0/Hybrid_0/Chip_{chip_id}/'
    obj = f'D_B(0)_O(0)_H(0)_TDAC1D_Chip({chip_id})'
    c = root_file.Get(f'{dir}/{obj}')
    for p in c.GetListOfPrimitives():
        if p.InheritsFrom('TH1') and p.GetName() == obj:
            h1_tdac = c.GetPrimitive(obj)
            return (h1_tdac.GetMean(), h1_tdac.GetRMS(), h1_tdac.GetSkewness())

    return (-1, -1, -1)


def checkSCurve(root_file, chip_id=0):
    dir = f'/Detector/Board_0/OpticalGroup_0/Hybrid_0/Chip_{chip_id}/'

    obj = f'D_B(0)_O(0)_H(0)_Threshold1D_Chip({chip_id})'
    c = root_file.Get(f'{dir}/{obj}')
    for p in c.GetListOfPrimitives():
        if p.InheritsFrom('TH1') and p.GetName() == obj:
            h1_threshold1D = c.GetPrimitive(obj)
            threshold_mean = h1_threshold1D.GetMean()

    obj = f'D_B(0)_O(0)_H(0)_Noise1D_Chip({chip_id})'
    c = root_file.Get(f'{dir}/{obj}')
    for p in c.GetListOfPrimitives():
        if p.InheritsFrom('TH1') and p.GetName() == obj:
            h1_noise1D = c.GetPrimitive(obj)
            noise_mean = h1_noise1D.GetMean()

    obj = f'D_B(0)_O(0)_H(0)_SCurves_Chip({chip_id})'
    c = root_file.Get(f'{dir}/{obj}')
    for p in c.GetListOfPrimitives():
        if p.InheritsFrom('TH2') and p.GetName() == obj:
            h2_scurves = c.GetPrimitive(obj)
            bin50 = h2_scurves.ProjectionY().FindBin(0.5)
            h1_scurve50 = h2_scurves.ProjectionX('_px', bin50, bin50+1)
            threshold_scurve_mean = h1_scurve50.GetMean()

    return threshold_mean, threshold_scurve_mean, noise_mean


class ThresholdSetup:
    def __init__(self, daqexe, rev_is_b=False, *, gdac_val=None, targetthr=None):
        self.gdac_val = gdac_val
        self.targetthr = targetthr
        if gdac_val is not None:
            self.dirname = f"threshold{gdac_val}gdac"
        if targetthr is not None:
            self.dirname = f"threshold{targetthr}e"
        os.mkdir(self.dirname) # Crash if the directory already exists.
        name = "CMSIT_RD53" + ("B" if rev_is_b else "A")
        shutil.copyfile(f"{name}.xml", f"{self.dirname}/CMSIT.xml")
        shutil.copyfile(f"{name}.txt", f"{self.dirname}/{name}.txt")
        self.daq = DAQ.DAQ(daqexe, self.dirname)
        self.tdac_central = 16 if rev_is_b else 8
        self.default_chip_id = 15 if rev_is_b else 0
        self.gdac_reg_name = "DAC_GDAC_M_LIN" if rev_is_b else "Vthreshold_LIN"


    def adjust(self, targetthr):
        logging.info("Running threshold adjustment")
        # prepare the XML config file for thradj
        self.daq.config_calibs({
            "nEvents": 100,
            "nEvtsBurst": 100,
            "INJtype": 1,
            "TargetThr": targetthr,
            # TODO ThrStart and ThrStop
        })

        # run CMSITminiDAQ and capture stdout
        data = self.daq.run_calib("thradj")

        # retrieve the values of VCAL_HIGH and VCAL_MED used by thradj
        vcals = re.compile(r"VCAL_HIGH.*\D(\d+)[^m\d].*VCAL_MED.*\D(\d+)[^m\d]")
        for line in data.stdout.decode().splitlines():
            match = vcals.search(line)
            if match is not None:
                vcalhigh = int(match.group(1))
                vcalmed = int(match.group(2))
                logging.debug(f"VCAL_HIGH={vcalhigh} ; VCAL_MED={vcalmed}")
                self.deltavcal = vcalhigh - vcalmed
                break

        # check that the value for VCAL_HIGH fits the register
        if vcalhigh > 4095:
            raise RuntimeError("The requested target threshold is too high!")

        # TODO support multiple threshold registers (for RD53B)

        # extract the best value for the threshold from stdout
        p = re.compile(r"Best.*m(.*_LIN).*\D(\d+)[^m\d]")
        for line in data.stdout.decode().splitlines():
            a = p.search(line)
            if a is not None:
                thr_reg = a.group(1)
                thrbest = int(a.group(2))
                break

        # check for convergence of thradj
        thrmin = int(self.daq.get_calib_param("ThrStart"))
        thrmax = int(self.daq.get_calib_param("ThrStop"))
        if thrbest in {thrmin, thrmax}:
            raise RuntimeError("Threshold adjustment failed to converge!")
        else:
            logging.info(f"Best value for {thr_reg}: {thrbest}")

        # update the XML config file
        self.daq.config_chips({thr_reg: thrbest})

        return vcalmed, vcalhigh, thrbest


    def equalize(self):
        logging.info("Running threshold equalization")

        # Retrieve the current run number (assigned to this next calibration).
        run_number = self.daq.get_run_number()

        # Run CMSITminiDAQ.
        output = self.daq.run_calib("threqu")

        # Find the value used by threqu for VCAL_HIGH
        p = re.compile(r"VCAL_HIGH.*\D(\d+)[^m\d]")
        vcalhighused = None
        for line in reversed(output.stdout.decode().splitlines()):
            a = p.search(line)
            if a is not None:
                vcalhighused = int(a.group(1))
                break

        # Check that the setting used for VCAL_HIGH was not one of the limits.
        vcalhighmin = int(self.daq.get_calib_param("VCalHstart"))
        vcalhighmax = int(self.daq.get_calib_param("VCalHstop"))
        if vcalhighused in {vcalhighmin, vcalhighmax} and (vcalhighmin != vcalhighmax):
            raise RuntimeError("Value of VCAL_HIGH for threqu was at limit!")
        else:
            logging.info(f"Value of VCAL_HIGH for threqu: {vcalhighused}")

        mean, rms, skewness = checkTDAC(ROOT.TFile(f"{self.dirname}/Results/Run{run_number}_ThrEqualization.root"), self.default_chip_id)
        logging.info(f"TDAC distribution: MEAN = {mean}, RMS = {rms}, SKEWNESS = {skewness}")

        print("Don't forget to check the TDAC distribution!")

        return vcalhighused


    def meas_noise(self):
        logging.info("Running noise measurement")

        # save
        original_vals = {}
        for option in ["nEvents", "nEvtsBurst", "INJtype"]:
            original_vals[option] = int(self.daq.get_calib_param(option))
        # update XML config file
        self.daq.config_calibs({
            "nEvents": 10000000,
            "nEvtsBurst": 10000,
            "INJtype": 0,
        })

        # run CMSITminiDAQ
        output = self.daq.run_calib("noise")

        #Find how many pixels were masked
        p = re.compile(r"potentially masked pixels.*\D(\d+)[^m\d]")
        for line in reversed(output.stdout.decode().splitlines()):
            a = p.search(line)
            if a is not None:
                masked = int(a.group(1))
                break
        logging.info(f"Number of pixels that were masked: {masked}")

        print("Don't forget to check the occupancy map!")

        self.daq.config_calibs(original_vals)

        return

    """Runs an S-curve measurement.

    Runs an S-curve measurement and, if requested, compares the threshold to its
    expected value, issuing a warning when they differ too much.

    Args:
        expectedthr: The expected value for the threshold in units of DeltaVCAL,
          i.e. in units of 1 LSB of the VCAL_HIGH/VCAL_MED DACs. If set to None,
          no comparison is done.
        maxdiff: The maximum difference in DeltaVCAL units between the expected
          and the measured threshold, for which no warning is generated.

    Returns:
        The measured threshold in DeltaVCAL units.
    """
    def meas_scurve(self, expectedthr=None, maxdiff=3):
        logging.info("Running S-curve measurement")

        # Retrieve the current run number (assigned to this next calibration).
        runnum = self.daq.get_run_number()

        # Run CMSITminiDAQ.
        self.daq.run_calib("scurve")

        # Extract information about the threshold from the S-curve.
        th1mean, _, _ = checkSCurve(ROOT.TFile(f"{self.dirname}/Results/Run{runnum}_SCurve.root"), self.default_chip_id)

        if expectedthr is None:
            return th1mean

        # Check if the expected and measured threshold are close.
        diff = th1mean - expectedthr
        if abs(diff) > maxdiff:
            logging.warning("Threshold from S-curve differs too much from expected value!")
        else:
            logging.debug(f"S-curve is ok (expected {expectedthr}, measured {th1mean} [DeltaVCAL])")

        return th1mean


    def prep_for_physics(self):
        logging.info("Configuring for physics data taking")

        self.daq.config_calibs({
            "INJtype": 0, #Disable all test injections.
            "nTRIGxEvent": 1 #Trigger only isolated bunch crossings.
        })

        # Remove the limit on the number of triggers.
        self.daq.config_fc7({
            "user.ctrl_regs.fast_cmd_reg_3.triggers_to_accept": 0,
        })

        return


    def run_all(self):
        logging.info(f"Configuring the chip with {self.gdac_reg_name} = {self.gdac_val}")
        self.daq.config_calibs({
            "ResetTDAC": self.tdac_central,
            "ResetMask": 0,
        })
        if self.gdac_val is not None:
            self.daq.config_chips({self.gdac_reg_name: self.gdac_val})
        if self.targetthr is not None:
            vcalmed, vcalhigh, _ = self.adjust(self.targetthr)
            self.daq.config_calibs({
                "VCalMED": vcalmed,
                "VCalHstart": max(vcalhigh, vcalmed),
                "VCalHstop": min(vcalhigh, 4095)
            })

        vcalhigh = self.equalize()
        vcalmed = int(self.daq.get_calib_param("VCalMED"))
        self.daq.config_calibs({"ResetTDAC": -1})
        self.meas_noise()
        self.daq.config_calibs({
            "VCalHstart": max(vcalhigh - 250, vcalmed),
            "VCalHstop": min(vcalhigh + 250, 4095)
        })
        self.meas_scurve(vcalhigh - vcalmed)
        self.prep_for_physics()
        shutil.move(self.dirname + "/Results", self.dirname + "/setupresults")
        shutil.move(self.dirname + "/logs", self.dirname + "/setuplogs")
        os.remove(self.dirname + "/RunNumber.txt")
        logging.info("Threshold setup complete")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("daq", help="Path to CMSITminiDAQ")
    parser.add_argument("--rd53b", action='store_true', help="Use it with RD53B")
    parser.add_argument("--gdac", type=int, help="Value of the global threshold DAC")
    parser.add_argument("--thr", type=int, help="Target threshold in electrons")
    args = parser.parse_args()
    if (args.gdac is None) and (args.thr is None):
        raise RuntimeError("Either --gdac or --thr must be provided!")
    logging.basicConfig(level=logging.DEBUG, format="%(asctime)s %(message)s", handlers=[logging.StreamHandler(), logging.FileHandler("ThresholdSetup.log")])
    tool = ThresholdSetup(args.daq, args.rd53b, gdac_val=args.gdac, targetthr=args.thr)
    tool.run_all()
