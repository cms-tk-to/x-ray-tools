>>> (Fe) 6400./3.6
1777.7777777777778
>>> (Cu) 8000./3.6
2222.222222222222
>>> (Ag) 23000./3.6
6388.888888888889
>>>

Fe: 1550e-1750e-1950e

Cu: 2000e-2300e-2500e

Ag: 6200e-6400-6600e

based on
https://indico.cern.ch/event/986962/sessions/388630/attachments/2202617/3726620/IT_Excercise.pdf
https://cms-tracker-daq.web.cern.ch/cms-tracker-daq/tutorials/ph2_acf/

<!-- #-------------------
# Step1: PixelAlive

CMSIT_Scurve.xml

<Setting name="nEvents">            100 </Setting>
<Setting name="nEvtsBurst">         100 </Setting>
<Setting name="INJtype">              1 </Setting>

CMSITminiDAQ –f CMSIT_Scurve.xml –c pixelalive
#------------------- -->

# Step 1: Threshold adjustment

CMSIT_Scurve.xml

<Setting name="nEvents">            100 </Setting>
<Setting name="nEvtsBurst">         100 </Setting>
<Setting name="INJtype">              1 </Setting>
<Setting name="TargetThr">         2000 </Setting>
<Setting name="ResetTDAC">            1 </Setting>

CMSITminiDAQ –f CMSIT_Scurve.xml –c thradj

Actions:

parse stdout for >>> Vthreshold_LIN value for [...] = xxx <<<
and check that the found value for xxx is not decreasing monotonically in all the steps (this means that binary search is failing)

if binary search is ok
parse stdout/stderr searching for "Global threshold for..."
and write the value found in Vthreshold_LIN

#-------------------
# Step 2: Threshold equalization

CMSIT_Scurve.xml

<Setting name="nEvents">            100 </Setting>
<Setting name="nEvtsBurst">         100 </Setting>
<Setting name="INJtype">              1 </Setting>
<Setting name="ResetTDAC">            1 </Setting>

CMSITminiDAQ –f CMSIT_Scurve.xml –c threqu


Actions: check if the distribution of TDAQ is symmetric
#-------------------
# Step 3: Noise

CMSIT_Noise.xml

<Setting name="nEvents">       10000000 </Setting>
<Setting name="nEvtsBurst">       10000 </Setting>
<Setting name="INJtype">              0 </Setting>
<Setting name="ResetTDAC">            0 </Setting>

CMSITminiDAQ –f CMSIT_Noise.xml –c noise

Actions:
Check occupancy map in the root file PixelAlive_Board00_Mod00_Chip00 oppure Occ1D_Board00_Mod00_Chip00

#-------------------
# Step 4: Threshold adjustment (2nd attempt)

CMSIT_Scurve.xml

<Setting name="nEvents">            100 </Setting>
<Setting name="nEvtsBurst">         100 </Setting>
<Setting name="INJtype">              1 </Setting>
<Setting name="TargetThr">         2000 </Setting>
<Setting name="ResetTDAC">            0 </Setting>

CMSITminiDAQ –f CMSIT_Scurve.xml –c thradj

Actions:

parse stdout for >>> Vthreshold_LIN value for [...] = xxx <<<
and check that the found value for xxx is not decreasing monotonically in all the steps (this means that binary search is failing)

if binary search is ok
parse stdout/stderr searching for "Global threshold for..."
and write the value found in Vthreshold_LIN (compare new vs. old value)


=> extract value of DeltaVCAL = (VCal_HIGH -VCal_MED)
#-------------------
# Step5: S-curve validation

CMSIT_Scurve.xml

<Setting name="nEvents">            100 </Setting>
<Setting name="nEvtsBurst">         100 </Setting>
<Setting name="INJtype">              1 </Setting>
<Setting name="VCalHstart">         100 </Setting>
<Setting name="VCalHstop">          600 </Setting>   << this can be changed
<Setting name="VCalHnsteps">         50 </Setting>
<Setting name="VCalMED">            100 </Setting>

CMSITminiDAQ –f CMSIT_Scurve.xml –c scurve

Actions:
"sanity check" - validation
which is the value of the threshold on this scale?
(VCalHStart) VCalHStop and VCalMed should be chosen such that target threshold is in the middle of the range
